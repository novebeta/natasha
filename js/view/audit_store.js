jun.Auditstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Auditstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AuditStoreId',
            url: 'Audit',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'audit_id'},
                {name: 'tgl'},
                {name: 'doc_ref'},
                {name: 'tdate'},
                {name: 'user_id'},
                {name: 'bruto'},
                {name: 'disc'},
                {name: 'discrp'},
                {name: 'ketdisc'},
                {name: 'vat'},
                {name: 'totalpot'},
                {name: 'total'},
                {name: 'customer_id'},
                {name: 'bank_id'},
                {name: 'doc_ref_sales'},
                {name: 'store'},
                {name: 'card_number'},
                {name: 'printed'}
            ]
        }, cfg));
    }
});
jun.rztAudit = new jun.Auditstore();
jun.Sales2Auditstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Sales2Auditstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'Sales2AuditstoreId',
            url: 'Audit/GetSalesTrans',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'salestrans_id'},
                {name: 'doc_ref'},
                {name: 'no_customer'},
                {name: 'nama_customer'},
                {name: 'nama_bank'},
                {name: 'cetak'},
                {name: 'total', type: 'float'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function (a) {
        var data = this.query('cetak', true);
        var total = 0;
        data.each(function (entry) {
            total += entry.data.total;
        });
//        data.forEach(logArrayElements);
//        forEach(data, function(value, key) {
//            console.log(key + ' = ' + value);
//        });
//        var total = this.sum("total");
        Ext.getCmp("totalauditid").setValue(total);
    }
});
jun.rztSales2Audit = new jun.Sales2Auditstore();
//jun.rztAudit.load();
