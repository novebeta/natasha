jun.BankTransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BankTransstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BankTransStoreId',
            url: 'BankTrans',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'bank_trans_id'},
                {name: 'type_'},
                {name: 'trans_no'},
                {name: 'ref'},
                {name: 'tgl'},
                {name: 'amount'},
                {name: 'id_user'},
                {name: 'tdate'},
                {name: 'bank_id'}
            ]
        }, cfg));
    }
});
jun.rztBankTrans = new jun.BankTransstore();
//jun.rztBankTrans.load();
