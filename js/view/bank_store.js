jun.Bankstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Bankstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BankStoreId',
            url: 'Bank',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'bank_id'},
                {name: 'nama_bank'},
                {name: 'ket'},
                {name: 'account_code'}
            ]
        }, cfg));
    }
});
jun.rztBank = new jun.Bankstore();
jun.rztBankLib = new jun.Bankstore();
jun.rztBankCmp = new jun.Bankstore({baseParams: {mode: "bank_cabang"}});
jun.rztBankCmpPusat = new jun.Bankstore({baseParams: {mode: "bank_pusat"}});
//jun.rztBankCmp.load();
