jun.Barangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Barangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BarangStoreId',
            url: 'Barang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'barang_id'},
                {name: 'kode_barang'},
                {name: 'nama_barang'},
                {name: 'ket'},
                {name: 'grup_id'},
                {name: 'active'},
                {name: 'price'},
                {name: 'sat'},
                {name: 'cost'}
            ]
        }, cfg));
    }
});
jun.rztBarang = new jun.Barangstore();
jun.rztBarangLib = new jun.Barangstore();
jun.rztBarangCmp = new jun.Barangstore();
jun.rztBarangJasa = new jun.Barangstore(
    {
        baseParams: {mode: "jasa"},
        method: 'POST'
    });
jun.rztBarangNonJasa = new jun.Barangstore(
    {
        baseParams: {mode: "nonjasa"},
        method: 'POST'
    });
//jun.rztBarang.load();
