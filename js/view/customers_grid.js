jun.ReportHistoryCustomersstore = Ext.extend(Ext.data.JsonReader, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ReportHistoryCustomersstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ReportHistoryCustomersstoreId',
            url: 'Report/ViewCustomerHistory',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'tgl'},
                {name: 'doc_ref'},
                {name: 'kode_barang'},
                {name: 'qty'},
                {name: 'sat'}
            ],
            remoteGroup: true,
            remoteSort: true
        }, cfg));
    }
});
jun.ReportHistoryCustomersGrid = Ext.extend(Ext.grid.GridPanel, {
    id: 'docs-jun.ReportHistoryCustomersGrid',
    height: 425,
    width: 495,
    stripeRows: true,
    resizable: !1,
    viewConfig: {
        forceFit: true
    },
    columns: [
        {
            header: 'No. Natasha Receipt',
            resizable: true,
            dataIndex: 'doc_ref',
            renderer: function (v, params, record) {
                return record.data.doc_ref + " / " + record.data.tgl;
            }
        },
        {
            header: 'Item Code',
            resizable: true,
            dataIndex: 'kode_barang'
        },
        {
            header: 'Qty',
            resizable: true,
            dataIndex: 'qty',
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Unit',
            resizable: true,
            dataIndex: 'sat'
        }
    ],
    initComponent: function () {
//        this.store = new jun.ReportHistoryCustomersstore({baseParams: {format: "html"}});
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    format: 'd M Y',
                    name: "tglfrom",
                    id: "tglfromReportHistoryCustomersGridid",
                    ref: '../tglfrom',
                    anchor: "100%"
                }
            ]
        };
        jun.ReportHistoryCustomersGrid.superclass.initComponent.call(this);
        this.tglfrom.on('select', function () {
            this.store.reload()
        }, this);
        this.store.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params = {
                        tglfrom: Ext.getCmp('tglfromReportHistoryCustomersGridid').getValue(),
                        customer_id: this.customer_id,
                        format: 'json'
                    }
                }
            }
        });
    }
})
;
jun.ReportHistoryCustomers = Ext.extend(Ext.Window, {
    title: "History Customer",
    iconCls: "silk13-report",
    modez: 1,
    width: 540,
    height: 600,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        var grid = new jun.ReportHistoryCustomersGrid({ds: new Ext.data.GroupingStore({
            reader: new jun.ReportHistoryCustomersstore({baseParams: {format: "html"}}),
            proxy: new Ext.data.HttpProxy({
                url: 'Report/ViewCustomerHistory',
                method: 'POST'
            }),
            sortInfo: {field: 'doc_ref', direction: 'DESC'},
            groupField: 'doc_ref'
        }),
            view: new Ext.grid.GroupingView({
                forceFit: true,
                showGroupName: false,
                enableNoGroups: false,
                enableGroupingMenu: false,
                hideGroupedColumn: true
            }),
            plugins: new Ext.ux.grid.HybridSummary(),
            customer_id: this.customer_id
        });
        this.items = [
            {
                xtype: "box",
                style: 'margin:5px',
                html: this.cust_label
            },
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-ReportHistoryCustomers",
                labelWidth: 100,
                labelAlign: "left",
                ref: "formz",
                border: !1,
                items: [
                    grid,
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Close",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ReportHistoryCustomers.superclass.initComponent.call(this);
        this.btnSave.on("click", function () {
            this.close()
        }, this);
    }
});
jun.CustomersGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Master Customers",
    id: 'docs-jun.CustomersGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Customers',
            sortable: true,
            resizable: true,
            dataIndex: 'no_customer',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Customer Name',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_customer',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Phone',
            sortable: true,
            resizable: true,
            dataIndex: 'telp',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Address',
            sortable: true,
            resizable: true,
            dataIndex: 'alamat',
            width: 100,
            filter: {xtype: "textfield"}
        }
        /*{
         header: 'Birthplace',
         sortable: true,
         resizable: true,
         dataIndex: 'tempat_lahir',
         width: 100,
         filter: {xtype: "textfield"}
         },
         {
         header: 'Birthday',
         sortable: true,
         resizable: true,
         dataIndex: 'tgl_lahir',
         width: 100,
         renderer: Ext.util.Format.dateRenderer('d M Y'),
         filter: {
         xtype: "xdatefield",
         format: 'd M Y'
         }
         },
         {
         header: 'email',
         sortable: true,
         resizable: true,
         dataIndex: 'email',
         width: 100
         },
         {
         header:'telp',
         sortable:true,
         resizable:true,
         dataIndex:'telp',
         width:100
         },
         {
         header:'alamat',
         sortable:true,
         resizable:true,
         dataIndex:'alamat',
         width:100
         },
         {
         header:'kota',
         sortable:true,
         resizable:true,
         dataIndex:'kota',
         width:100
         },
         {
         header:'negara',
         sortable:true,
         resizable:true,
         dataIndex:'negara',
         width:100
         },
         {
         header:'awal',
         sortable:true,
         resizable:true,
         dataIndex:'awal',
         width:100
         },
         {
         header:'akhir',
         sortable:true,
         resizable:true,
         dataIndex:'akhir',
         width:100
         },
         */
    ],
    initComponent: function () {
        if (jun.rztNegaraCmp.getTotalCount() === 0) {
            jun.rztNegaraCmp.load();
        }
        if (jun.rztProvinsiCmp.getTotalCount() === 0) {
            jun.rztProvinsiCmp.load();
        }
        if (jun.rztKotaCmp.getTotalCount() === 0) {
            jun.rztKotaCmp.load();
        }
        if (jun.rztKecamatanCmp.getTotalCount() === 0) {
            jun.rztKecamatanCmp.load();
        }
        if (jun.rztStatusCustCmp.getTotalCount() === 0) {
            jun.rztStatusCustCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.store = jun.rztCustomers;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Customer',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Customer',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    fieldLabel: 'Date',
                    xtype: 'xdatefield',
                    format: 'd M Y',
                    ref: '../tgl',
                    anchor: "100%"
                },
                {
                    xtype: 'button',
                    text: 'View History',
                    ref: '../btnHistory'
                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-ReportViewHistoryCustomers",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "customer_id",
                            ref: "../../customer_id"
                        },
                        {
                            xtype: "hidden",
                            name: "tglfrom",
                            ref: "../../tglfrom"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            ref: "../../format"
                        }
                    ]
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.CustomersGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnHistory.on('Click', this.onbtnHistoryclick, this);
        this.on('rowdblclick', this.viewForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    viewForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a customer");
            return;
        }
        var idz = selectedz.json.customer_id;
        var form = new jun.CustomersWin({modez: 2, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    onbtnHistoryclick: function () {
        Ext.Ajax.request({
            url: 'customers/createlog',
            method: 'POST',
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                $.ajax({
                    url: response.url,
                    type: "POST",
                    crossDomain: true,
                    data: response,
                    dataType: "json",
                    success:function(result){
                        alert(JSON.stringify(result));
                    },
                    error:function(xhr,status,error){
                        alert(status);
                    }
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

        return;
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Customer");
            return;
        }
        var idz = selectedz.json.customer_id;
//        var form = new jun.ReportHistoryCustomers({
//            customer_id: idz,
//            cust_label: "Customer Number : " + selectedz.json.no_customer +
//                "<br>Customer Name : " + selectedz.json.nama_customer +
//                "<br>Birthday : " + selectedz.json.tgl_lahir +
//                "<br>Phone : " + selectedz.json.telp +
//                "<br>Address : " + selectedz.json.alamat
//        });
//        form.show();
        Ext.getCmp("form-ReportViewHistoryCustomers").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportViewHistoryCustomers").getForm().url = "Report/ViewCustomerHistory";
        this.customer_id.setValue(idz);
        this.format.setValue('html');
        this.tglfrom.setValue(this.tgl.getValue().format('Y-m-d'));
        var form = Ext.getCmp('form-ReportViewHistoryCustomers').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.CustomersWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a customer");
            return;
        }
        var idz = selectedz.json.customer_id;
        var form = new jun.CustomersWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a customer");
            return;
        }
        Ext.Ajax.request({
            url: 'Customers/delete/id/' + record.json.customer_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztCustomers.reload();
                jun.rztCustomersCmp.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})
