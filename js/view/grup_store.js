jun.Grupstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Grupstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'GrupStoreId',
            url: 'Grup',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'grup_id'},
                {name: 'nama_grup'},
                {name: 'kategori_id'},
                {name: 'vat'},
                {name: 'tax'},
                {name: 'coa_jual'}
            ]
        }, cfg));
    }
});
jun.rztGrup = new jun.Grupstore();
jun.rztGrupJasa = new jun.Grupstore({
    baseParams: {mode: "jasa"},
    method: 'POST'
});
//jun.rztGrup.load();
