jun.Kategoristore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Kategoristore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KategoriStoreId',
            url: 'Kategori',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'kategori_id'},
                {name: 'nama_kategori'}
            ]
        }, cfg));
    }
});
jun.rztKategori = new jun.Kategoristore();
//jun.rztKategori.load();
