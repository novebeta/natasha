jun.Provinsistore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Provinsistore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ProvinsiStoreId',
            url: 'Provinsi',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'provinsi_id'},
                {name: 'nama_provinsi'},
                {name: 'negara_id'}
            ]
        }, cfg));
    }
});
jun.rztProvinsi = new jun.Provinsistore();
jun.rztProvinsiLib = new jun.Provinsistore();
jun.rztProvinsiCmp = new jun.Provinsistore();
//jun.rztProvinsi.load();
