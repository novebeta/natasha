jun.ReportNewCustomers = Ext.extend(Ext.Window, {
    title: "New Customers",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-ReportNewCustomers",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportNewCustomers.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportNewCustomers").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportNewCustomers").getForm().url = "Report/NewCustomers";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportNewCustomers').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportNewCustomers").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportNewCustomers").getForm().url = "Report/NewCustomers";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportNewCustomers').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportBiaya = Ext.extend(Ext.Window, {
    title: "Cash Out",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-ReportBiaya",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportBiaya.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportBiaya").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBiaya").getForm().url = "Report/CashOut";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportBiaya').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportBiaya").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBiaya").getForm().url = "Report/CashOut";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportBiaya').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportInventoryMovements = Ext.extend(Ext.Window, {
    title: "Inventory Movements",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-ReportInventoryMovements",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportInventoryMovements.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportInventoryMovements").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInventoryMovements").getForm().url = "Report/InventoryMovements";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportInventoryMovements').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportInventoryMovements").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInventoryMovements").getForm().url = "Report/InventoryMovements";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportInventoryMovements').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportInventoryCard = Ext.extend(Ext.Window, {
    title: "Inventory Card",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztBarangNonJasa.getTotalCount() === 0) {
            jun.rztBarangNonJasa.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-ReportInventoryCard",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Item',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztBarangNonJasa,
                        hiddenName: 'barang_id',
                        valueField: 'barang_id',
                        ref: '../barang',
                        displayField: 'kode_barang',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportInventoryCard.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportInventoryCard").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInventoryCard").getForm().url = "Report/InventoryCard";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportInventoryCard').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportInventoryCard").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInventoryCard").getForm().url = "Report/InventoryCard";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportInventoryCard').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportBeautySummary = Ext.extend(Ext.Window, {
    title: "Beautician Services Summary",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-ReportBeautySummary",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportBeautySummary.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportBeautySummary").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBeautySummary").getForm().url = "Report/BeautySummary";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportBeautySummary').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.Ajax.request({
            url: 'BeautyServices/CheckFinal',
            method: 'POST',
            scope: this,
            params: {
                tglfrom: this.tglfrom.getValue(),
                tglto: this.tglto.getValue()
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (response.success == false) {
                    Ext.MessageBox.show({
                        title: 'Fatal Error',
                        msg: response.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                    return;
                }
                Ext.getCmp("form-ReportBeautySummary").getForm().standardSubmit = !0;
                Ext.getCmp("form-ReportBeautySummary").getForm().url = "Report/BeautySummary";
                this.format.setValue("excel");
                var form = Ext.getCmp('form-ReportBeautySummary').getForm();
                var el = form.getEl().dom;
                var target = document.createAttribute("target");
                target.nodeValue = "_blank";
                el.setAttributeNode(target);
                el.action = form.url;
                el.submit();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.ReportBeautyDetails = Ext.extend(Ext.Window, {
    title: "Beautician Services Details",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-ReportBeautyDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportBeautyDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportBeautyDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBeautyDetails").getForm().url = "Report/BeautyDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportBeautyDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.Ajax.request({
            url: 'BeautyServices/CheckFinal',
            method: 'POST',
            scope: this,
            params: {
                tglfrom: this.tglfrom.getValue(),
                tglto: this.tglto.getValue()
            },
            success: function (f, a) {
                Ext.Ajax.request({
                    url: 'BeautyServices/CheckFinal',
                    method: 'POST',
                    scope: this,
                    params: {
                        tglfrom: this.tglfrom.getValue(),
                        tglto: this.tglto.getValue()
                    },
                    success: function (f, a) {
                        var response = Ext.decode(f.responseText);
                        if (response.success == false) {
                            Ext.MessageBox.show({
                                title: 'Fatal Error',
                                msg: response.msg,
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                            return;
                        }
                        Ext.getCmp("form-ReportBeautyDetails").getForm().standardSubmit = !0;
                        Ext.getCmp("form-ReportBeautyDetails").getForm().url = "Report/BeautyDetails";
                        this.format.setValue("excel");
                        var form = Ext.getCmp('form-ReportBeautyDetails').getForm();
                        var el = form.getEl().dom;
                        var target = document.createAttribute("target");
                        target.nodeValue = "_blank";
                        el.setAttributeNode(target);
                        el.action = form.url;
                        el.submit();
                    },
                    failure: function (f, a) {
                        switch (a.failureType) {
                            case Ext.form.Action.CLIENT_INVALID:
                                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                break;
                            case Ext.form.Action.CONNECT_FAILURE:
                                Ext.Msg.alert('Failure', 'Ajax communication failed');
                                break;
                            case Ext.form.Action.SERVER_INVALID:
                                Ext.Msg.alert('Failure', a.result.msg);
                        }
                    }
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.ReportSalesSummary = Ext.extend(Ext.Window, {
    title: "Sales Summary",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-ReportSalesSummary",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Group',
                        store: jun.rztGrup,
                        ref: '../grup',
                        allowBlank: true,
                        emptyText: 'ALL Group',
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        displayField: 'nama_grup',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportSalesSummary.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSalesSummary").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSalesSummary").getForm().url = "Report/SalesSummary";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportSalesSummary').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportSalesSummary").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSalesSummary").getForm().url = "Report/SalesSummary";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportSalesSummary').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportSalesDetails = Ext.extend(Ext.Window, {
    title: "Sales Details",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-ReportSalesDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Group',
                        store: jun.rztGrup,
                        ref: '../grup',
                        allowBlank: true,
                        emptyText: 'ALL Group',
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        displayField: 'nama_grup',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportSalesDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSalesDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSalesDetails").getForm().url = "Report/SalesDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportSalesDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportSalesDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSalesDetails").getForm().url = "Report/SalesDetails";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportSalesDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportReturSalesSummary = Ext.extend(Ext.Window, {
    title: "Return Sales Summary",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-ReportReturSalesSummary",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Group',
                        store: jun.rztGrup,
                        ref: '../grup',
                        allowBlank: true,
                        emptyText: 'ALL Group',
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        displayField: 'nama_grup',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportReturSalesSummary.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportReturSalesSummary").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportReturSalesSummary").getForm().url = "Report/ReturSalesSummary";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportReturSalesSummary').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportReturSalesSummary").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportReturSalesSummary").getForm().url = "Report/ReturSalesSummary";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportReturSalesSummary').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportReturSalesDetails = Ext.extend(Ext.Window, {
    title: "Return Sales Details",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-ReportReturSalesDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Group',
                        store: jun.rztGrup,
                        ref: '../grup',
                        allowBlank: true,
                        emptyText: 'ALL Group',
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        displayField: 'nama_grup',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportReturSalesDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportReturSalesDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportReturSalesDetails").getForm().url = "Report/ReturSalesDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportReturSalesDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportReturSalesDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportReturSalesDetails").getForm().url = "Report/ReturSalesDetails";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportReturSalesDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportLaha = Ext.extend(Ext.Window, {
    title: "Daily Report",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 115,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-ReportLaha",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-html",
//                    text: "Show HTML",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportLaha.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportLaha").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportLaha").getForm().url = "Report/Laha";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportLaha').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportLaha").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportLaha").getForm().url = "Report/Laha";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportLaha').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportTender = Ext.extend(Ext.Window, {
    title: "Tenders",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 115,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-ReportTender",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportTender.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportTender").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportTender").getForm().url = "Report/tenders";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportTender').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportTender").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportTender").getForm().url = "Report/tenders";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportTender').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportSalesSummaryReceipt = Ext.extend(Ext.Window, {
    title: "Sales Summary Receipt",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-ReportSalesSummaryReceipt",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportSalesSummaryReceipt.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSalesSummaryReceipt").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSalesSummaryReceipt").getForm().url = "Report/SalesSummaryReceipt";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportSalesSummaryReceipt').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportSalesSummaryReceipt").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSalesSummaryReceipt").getForm().url = "Report/SalesSummaryReceipt";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportSalesSummaryReceipt').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportSalesSummaryReceiptDetails = Ext.extend(Ext.Window, {
    title: "Sales Summary Receipt Details",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-ReportSalesSummaryReceiptDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportSalesSummaryReceiptDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSalesSummaryReceiptDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSalesSummaryReceiptDetails").getForm().url = "Report/SalesSummaryReceiptDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportSalesSummaryReceiptDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportSalesSummaryReceiptDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSalesSummaryReceiptDetails").getForm().url = "Report/SalesSummaryReceiptDetails";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportSalesSummaryReceiptDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportDokterSummary = Ext.extend(Ext.Window, {
    title: "Doctor Summary",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-ReportDokterSummary",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportDokterSummary.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportDokterSummary").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportDokterSummary").getForm().url = "Report/DokterSummary";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportDokterSummary').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportDokterSummary").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportDokterSummary").getForm().url = "Report/DokterSummary";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportDokterSummary').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportDokterDetails = Ext.extend(Ext.Window, {
    title: "Doctor Details",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-ReportDokterDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportDokterDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportDokterDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportDokterDetails").getForm().url = "Report/DokterDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportDokterDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportDokterDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportDokterDetails").getForm().url = "Report/DokterDetails";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportDokterDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportGeneralLedger = Ext.extend(Ext.Window, {
    title: "General Ledger",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztChartMasterCmp.getTotalCount() === 0) {
            jun.rztChartMasterCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-ReportGeneralLedger",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'COA',
                        store: jun.rztChartMasterCmp,
                        hiddenName: 'account_code',
                        valueField: 'account_code',
                        forceSelection: true,
                        matchFieldWidth: !1,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                            "</div></tpl>"),
                        listWidth: 500,
                        displayField: 'account_code',
                        ref: '../cmbkode',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportGeneralLedger.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportGeneralLedger").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportGeneralLedger").getForm().url = "Report/GeneralLedger";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportGeneralLedger').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportGeneralLedger").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportGeneralLedger").getForm().url = "Report/GeneralLedger";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportGeneralLedger').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportGeneralJournal = Ext.extend(Ext.Window, {
    title: "General Journal",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-ReportGeneralJournal",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportGeneralJournal.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportGeneralJournal").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportGeneralJournal").getForm().url = "Report/GeneralJournal";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportGeneralJournal').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportGeneralJournal").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportGeneralJournal").getForm().url = "Report/GeneralJournal";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportGeneralJournal').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});