jun.returSalestransdetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.returSalestransdetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'returSalestransdetailsStoreId',
            url: 'ReturSalestrans/Detail',
            autoLoad: !1,
            autoSave: !1,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'salestrans_details'},
                {name: 'barang_id'},
                {name: 'kode_barang'},
                {name: 'nama_barang'},
                {name: 'salestrans_id'},
                {name: 'qty', type: 'float'},
                {name: 'beauty_id'},
                {name: 'disc', type: 'float'},
                {name: 'discrp', type: 'float'},
                {name: 'ketpot'},
                {name: 'vat', type: 'float'},
                {name: 'vatrp', type: 'float'},
                {name: 'beauty_tip', type: 'float'},
                {name: 'bruto', type: 'float'},
                {name: 'total', type: 'float'},
                {name: 'total_pot', type: 'float'},
                {name: 'price', type: 'float'},
                {name: 'jasa_dokter', type: 'float'},
                {name: 'dokter_id'},
                {name: 'disc_name'},
                {name: 'disc1', type: 'float'},
                {name: 'discrp1', type: 'float'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function (a) {
        var subtotal = this.sum("total");
        var vatrp = this.sum("vatrp");
        var totalpot = this.sum("discrp1");
        var bayar = parseFloat(Ext.getCmp("bayarid").getValue());
//        var discrp = parseFloat(Ext.getCmp("discrpid").getValue());
        Ext.getCmp("totalpotid").setValue(totalpot);
        Ext.getCmp("subtotalid").setValue(subtotal);
        Ext.getCmp("vatid").setValue(vatrp);
        var total = subtotal  - totalpot + vatrp;
        Ext.getCmp("totalid").setValue(total);
        var kembali = bayar - total;
        Ext.getCmp("kembaliid").setValue(kembali);
    }
});
jun.rztReturSalestransDetails = new jun.returSalestransdetailsstore();
//jun.rztSalestransDetails.load();
