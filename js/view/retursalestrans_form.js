jun.ReturSalestransWin = Ext.extend(Ext.Window, {
    title: 'Return Sales',
    modez: 1,
    width: 945,
    height: 565,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'padding: 10px',
                id: 'form-Salestrans',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Date',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        readOnly: true,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. Receipt',
                        hideLabel: false,
                        hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 20,
//                        x: 85,
//                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },

                    {
                        xtype: "label",
                        text: "Sales Receipt :",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. Receipt',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref_sales',
                        id: 'doc_ref_salesid',
                        ref: '../doc_ref_sales',
                        maxLength: 20,
                        x: 400,
                        y: 32,
                        height: 20,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Customer:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'combo',
//                        typeAhead: true,
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'remote',
                        autoSelect: false,
                        id: "customersales_id",
                        store: jun.rztCustomersCmp,
                        hiddenName: 'customer_id',
                        valueField: 'customer_id',
                        displayField: 'nama_customer',
                        forceSelection: true,
                        hideTrigger: true,
                        minChars: 3,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span>{telp} <br /> {tgl_lahir:date("M j, Y")}</span>{no_customer} | {nama_customer}</h3>',
                            '{alamat}',
                            "</div></tpl>"),
                        allowBlank: false,
                        listWidth: 750,
                        x: 85,
                        y: 32,
                        height: 20,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Payment Method:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztBankCmp,
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        value: SYSTEM_BANK_CASH,
                        readOnly: true,
                        width: 175,
                        x: 400,
                        y: 2
                    },
                    new jun.ReturSalestransDetailsGrid({
                        x: 5,
                        y: 65,
                        height: 285,
                        frameHeader: !1,
                        header: !1
                    }),
                    {
                        xtype: "label",
                        text: "Sub Total:",
                        x: 295,
                        y: 365
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'bruto',
                        id: 'subtotalid',
                        ref: '../subtotal',
                        maxLength: 30,
                        readOnly: true,
                        width: 175,
                        x: 400,
                        y: 362
                    },
                    {
                        xtype: "label",
                        text: "VAT:",
                        x: 295,
                        y: 395
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'vat',
                        id: 'vatid',
                        ref: '../vat',
                        maxLength: 30,
                        readOnly: true,
                        width: 175,
                        x: 400,
                        y: 392
                    },
                    {
                        xtype: "label",
                        text: "Disc (%):",
                        x: 295,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'disc',
                        id: 'discid',
                        ref: '../disc',
                        maxLength: 3,
                        value: 0,
                        minValue: 0,
                        maxValue: 100,
                        width: 175,
                        readOnly: true,
                        enableKeyEvents:true,
                        x: 400,
                        y: 422
                    },
                    {
                        xtype: "label",
                        text: "Disc Amount:",
                        x: 295,
                        y: 455
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'discrp',
                        id: 'discrpid',
                        ref: '../discrp',
                        value: 0,
                        maxLength: 30,
                        width: 175,
                        readOnly: true,
                        enableKeyEvents:true,
                        x: 400,
                        y: 452
                    },
                    {
                        xtype: "label",
                        text: "Comment:",
                        x: 610,
                        y: 365
                    },
                    {
                        xtype: 'textarea',
                        enableKeyEvents: true,
                        style : {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'ketdisc',
                        id: 'ketdiscid',
                        ref: '../ketdisc',
                        maxLength: 255,
                        width: 200,
                        height: 25,
                        x: 715,
                        y: 365
                    },
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 610,
                        y: 392
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'total',
                        id: 'totalid',
                        ref: '../total',
                        maxLength: 30,
                        width: 200,
                        x: 715,
                        y: 392
                    },
                    {
                        xtype: "label",
                        text: "Collect:",
                        x: 610,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'bayar',
                        id: 'bayarid',
                        ref: '../bayar',
                        enableKeyEvents:true,
                        value: 0,
                        maxLength: 30,
                        width: 200,
                        x: 715,
                        y: 422
                    },
                    {
                        xtype: "label",
                        text: "Change:",
                        x: 610,
                        y: 455
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'kembali',
                        id: 'kembaliid',
                        ref: '../kembali',
                        value: 0,
                        readOnly: true,
                        maxLength: 30,
                        width: 200,
                        x: 715,
                        y: 452
                    },
                    {
                        xtype: 'hidden',
                        id: 'overrideid',
                        ref: '../override'
                    },
                    {
                        xtype: 'hidden',
                        id: 'totalpotid',
                        ref: '../totalpot',
                        name: 'totalpot'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ReturSalestransWin.superclass.initComponent.call(this);
        this.disc.on('keyup', this.onDiscChange, this);
        this.discrp.on('keyup', this.onDiscrpChange, this);
        this.bayar.on('keyup', this.onBayarChange, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
            this.setDateTime();
        }
        if (SALES_OVERRIDE == "1") {
            this.disc.setReadOnly(false);
            this.discrp.setReadOnly(false);
        } else {
            this.disc.setReadOnly(true);
            this.discrp.setReadOnly(true);
            Ext.getCmp('discid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
            Ext.getCmp('discrpid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
        }
    },
    setDateTime: function () {
        Ext.Ajax.request({
            url: 'site/GetDateTime',
            method: 'POST',
            scope: this,
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                this.tgl.setValue(Date.parseDate(response.datetime, 'Y-m-d H:i:s'));
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onWinClose: function () {
        jun.rztReturSalestransDetails.removeAll();
    },
    onBayarChange: function(){
        jun.rztReturSalestransDetails.refreshData();
    },
    onDiscChange: function (a) {
        var disc1 = parseFloat(a.getValue());
//        if (disc1 == 0) return;
        var discrpf = parseFloat(this.discrp.getValue());
        if (discrpf != 0) {
            this.discrp.setValue(0);
        }
        jun.rztReturSalestransDetails.each(function (record) {
            var barang = jun.getBarang(record.data.barang_id);
            var price = parseFloat(record.data.price);
            var qty = parseFloat(record.data.qty);
            var disc = parseFloat(record.data.disc);
            var discrp = parseFloat(record.data.discrp);
            var bruto = price * qty;
            var vat = jun.getTax(record.data.barang_id);
            discrp = disc == 0 ? discrp : (disc / 100) * bruto;
            var subtotal = bruto - discrp;
            var discrp1 = disc1 == 0 ? 0 : (disc1 / 100) * subtotal;
            var totalpot = discrp + discrp1;
            var total_with_disc = bruto - totalpot;
            var total = bruto - discrp;
            var vatrp = total_with_disc * vat;
            record.set('discrp', discrp);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('total_pot', totalpot);
            record.set('total', total);
            record.set('bruto', bruto);
            record.set('disc1', disc1);
            record.set('discrp1', discrp1);
            record.commit();
        });
    },
    onDiscrpChange: function (a) {
        var discrpf = parseFloat(a.getValue());
        Ext.getCmp('btnsalesdetilid').setDisabled(discrpf != 0);
        var discf = parseFloat(this.disc.getValue());
        if (discf != 0) {
            this.disc.setValue(0);
            var disc1 = 0;
            var discrp1 = 0;
            jun.rztReturSalestransDetails.each(function (record) {
                var barang = jun.getBarang(record.data.barang_id);
                var price = parseFloat(record.data.price);
                var qty = parseFloat(record.data.qty);
                var disc = parseFloat(record.data.disc);
                var discrp = parseFloat(record.data.discrp);
                var bruto = price * qty;
                var vat = jun.getTax(record.data.barang_id);
                discrp = disc == 0 ? discrp : (disc / 100) * bruto;
                var subtotal = bruto - discrp;
                var totalpot = discrp + discrp1;
                var total_with_disc = bruto - totalpot;
                var total = bruto - discrp;
                var vatrp = total_with_disc * vat;
                record.set('discrp', discrp);
                record.set('vat', vat);
                record.set('vatrp', vatrp);
                record.set('total_pot', totalpot);
                record.set('total', total);
                record.set('bruto', bruto);
                record.set('disc1', disc1);
                record.set('discrp1', discrp1);
                record.commit();
            });
        }
        var subtotalf = jun.rztReturSalestransDetails.sum('total');
        if (subtotalf == 0)return;
        jun.rztReturSalestransDetails.each(function (record) {
            var total = parseFloat(record.data.total);
            var disc1 = (total / subtotalf) * 100;
            var barang = jun.getBarang(record.data.barang_id);
            var price = parseFloat(record.data.price);
            var qty = parseFloat(record.data.qty);
            var disc = parseFloat(record.data.disc);
            var discrp = parseFloat(record.data.discrp);
            var bruto = price * qty;
            var vat = jun.getTax(record.data.barang_id);
            discrp = disc == 0 ? discrp : (disc / 100) * bruto;
            var subtotal = bruto - discrp;
            var discrp1 = disc1 == 0 ? 0 : (disc1 / 100) * discrpf;
            var totalpot = discrp + discrp1;
            var total_with_disc = bruto - totalpot;
            total = bruto - discrp;
            var vatrp = total_with_disc * vat;
            record.set('discrp', discrp);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('total_pot', totalpot);
            record.set('total', total);
            record.set('bruto', bruto);
            record.set('disc1', disc1);
            record.set('discrp1', discrp1);
            record.commit();
        });
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var kembali = parseFloat(this.kembali.getValue());
        if(kembali < 0){
            Ext.Msg.alert('Error', "Change can't less than 0");
            this.btnDisabled(false);
            return;
        }
        var urlz = 'ReturSalestrans/create/';
        Ext.getCmp('form-Salestrans').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztReturSalestransDetails.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztReturSalestrans.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.closeForm) {
                    this.close();
                } else {
                    Ext.getCmp('form-Salestrans').getForm().reset();
                    this.setDateTime();
                    this.onWinClose();
                    jun.rztReturSalestransDetails.refreshData();
                    this.btnDisabled(false);
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});