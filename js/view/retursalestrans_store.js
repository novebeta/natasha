jun.Retursalestransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Retursalestransstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'RetursalestransstoreId',
            url: 'retursalestrans',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'salestrans_id'},
                {name: 'tgl'},
                {name: 'doc_ref'},
                {name: 'tdate'},
                {name: 'user_id'},
                {name: 'bruto'},
                {name: 'disc'},
                {name: 'discrp'},
                {name: 'totalpot'},
                {name: 'total'},
                {name: 'ketdisc'},
                {name: 'vat'},
                {name: 'customer_id'},
                {name: 'bank_id'},
                {name: 'doc_ref_sales'},
                {name: 'audit'},
                {name: 'store'},
                {name: 'printed'},
                {name: 'override'},
                {name: 'nama_customer'},
                {name: 'no_customer'},
                {name: 'bayar'},
                {name: 'kembali'}
            ]
        }, cfg));
    }
});
jun.rztReturSalestrans = new jun.Retursalestransstore();