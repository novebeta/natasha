jun.SecurityRolesWin = Ext.extend(Ext.Window, {
    title: "Security Role",
    modez: 1,
    width: 500,
    height: 500,
    layout: "form",
    modal: !0,
    padding: 3,
    resizable: !1,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-SecurityRoles",
                labelWidth: 100,
                labelAlign: "left",
                layout: "accordion",
                ref: "formz",
                border: !1,
                anchor: "100% 100%",
                items: [
                    {
                        xtype: "panel",
                        title: "Description",
                        layout: "form",
                        bodyStyle: "padding: 10px",
                        items: [
                            {
                                xtype: "textfield",
                                fieldLabel: "Role Name",
                                hideLabel: !1,
                                name: "role",
                                id: "roleid",
                                ref: "../role",
                                maxLength: 30,
                                anchor: "100%"
                            },
                            {
                                xtype: "textarea",
                                fieldLabel: "Description",
                                enableKeyEvents: true,
                                style : {textTransform: "uppercase"},
                                listeners: {
                                    change: function (field, newValue, oldValue) {
                                        field.setValue(newValue.toUpperCase());
                                    }
                                },
                                hideLabel: !1,
                                name: "ket",
                                id: "ketid",
                                ref: "../ket",
                                maxLength: 255,
                                anchor: "100%"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Master Section",
                        layout:'column',
                        bodyStyle: "padding: 10px;",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .5,
                                boxLabel: "Country",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "100"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Province",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "101"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "City",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "102"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Sub District",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "103"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Doctor",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "104"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Bank",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "105"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Group",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "106"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Items",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "107"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Customers",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "108"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Beautician",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "109"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Beauty Tips Class",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "110"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Value Tips Class",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "111"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Reference",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "112"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Status Customers",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "113"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Discount",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "114"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Store",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "115"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Supplier",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "116"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Chart Of Account",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "117"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Default Purchase Price",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "118"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Transaction Section",
                        layout:'column',
                        bodyStyle: "padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .5,
                                boxLabel: "Sales",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "200"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Return Sales",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "201"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Service",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "202"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Cash In",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "203"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Cash Out",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "204"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Purchase Item",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "205"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Return Supplier Item",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "206"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Tender Declaration",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "207"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Audit",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "208"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Override Price & Discount",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "209"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "History",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "210"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "General Journal",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "211"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Cash/Bank HO Deposit",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "212"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Cash/Bank HO Payment",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "213"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Cash/Bank Transfer",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "214"
                            }

                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Report Section",
                        layout:'column',
                        bodyStyle: "padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .5,
                                boxLabel: "Sales",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "313"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Sales Summary",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "300"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Sales Details",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "301"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Inventory Movement",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "302"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Inventory Card",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "303"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Beautician Services Summary",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "304"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Beautician Services Details",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "305"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Doctors Services Summary",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "311"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Doctors Services Details",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "312"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "New Customers",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "306"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Tenders",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "307"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Daily Report",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "308"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Return Sales Summary",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "309"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Return Sales Details",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "310"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "General Ledger",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "314"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "General Journal",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "315"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Administration Section",
                        layout:'column',
                        bodyStyle: "padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .5,
                                boxLabel: "User Management",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "400"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Security Roles",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "401"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Backup / Restore",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "402"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Import",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "403"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "General Section",
                        layout:'column',
                        bodyStyle: "padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .5,
                                boxLabel: "Change Password",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "000"
                            },
                            {
                                columnWidth: .5,
                                boxLabel: "Logout",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "001"
                            }
                        ]
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Save & Close",
                    ref: "../btnSaveClose"
                },
                {
                    xtype: "button",
                    text: "Close",
                    ref: "../btnCancel"
                }
            ]
        };
        jun.SecurityRolesWin.superclass.initComponent.call(this);
        this.on("activate", this.onActivate, this);
        this.btnSaveClose.on("click", this.onbtnSaveCloseClick, this);
//        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
    },
    btnDisabled: function (a) {
        this.btnSaveClose.setDisabled(a);
    },
    onActivate: function () {
        this.btnSaveClose.hidden = !1;
    },
    saveForm: function () {
        this.btnDisabled(!0);
        var a;
        this.modez == 1 || this.modez == 2 ? a = "SecurityRoles/update/id/" + this.id : a = "SecurityRoles/create/",
            Ext.getCmp("form-SecurityRoles").getForm().submit({
                url: a,
                timeOut: 1e3,
                scope: this,
                success: function (a, b) {
                    jun.rztSecurityRoles.reload();
                    jun.sidebar.getRootNode().reload();
                    var c = Ext.decode(b.response.responseText);
                    this.close();
//                    this.closeForm ? this.close() : (c.data != undefined && Ext.MessageBox.alert("Pelayanan", c.data.msg),
//                        this.modez == 0 && Ext.getCmp("form-SecurityRoles").getForm().reset());
                },
                failure: function (a, b) {
                    Ext.MessageBox.alert("Error", "Can't Communicate With The Server");
                    this.btnDisabled(!1);
                }
            });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = !0, this.saveForm(!0);
    },
    onbtnSaveclick: function () {
        this.closeForm = !1, this.saveForm(!1);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});