jun.StatusCuststore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.StatusCuststore.superclass.constructor.call(this, Ext.apply({
            storeId: 'StatusCustStoreId',
            url: 'StatusCust',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'status_cust_id'},
                {name: 'nama_status'},
            ]
        }, cfg));
    }
});
jun.rztStatusCust = new jun.StatusCuststore();
jun.rztStatusCustLib = new jun.StatusCuststore();
jun.rztStatusCustCmp = new jun.StatusCuststore();
//jun.rztStatusCust.load();
