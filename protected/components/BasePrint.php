<?php
/**
 * Created by PhpStorm.
 * User: MASTER
 * Date: 7/12/14
 * Time: 11:30 AM
 */

class BasePrint {
    function fillWithChar($char, $l = CHARLENGTHRECEIPT)
    {
        $res = "";
        for ($i = 0; $i < $l; $i++) {
            $res .= $char;
        }
        return $res;
    }
    function setCenter($msg, $l = CHARLENGTHRECEIPT)
    {
        $lmsg = strlen($msg);
        if ($msg > $l) {
            return substr($msg, 0, $l);
        }
        $sisa = $l - $lmsg;
        $awal = $sisa >> 1;
        $res = self::fillWithChar(" ", $awal);
        $res .= $msg . self::fillWithChar(" ", $sisa - $awal);
        return $res;
    }
    function addHeaderSales($msg1, $msg2, $l = 15)
    {
        $lmsg1 = strlen($msg1);
        if ($lmsg1 > $l) {
            $msg1 = substr($msg1, 0, $l);
            $lmsg1 = 15;
        }
        $res = $msg1 . self::fillWithChar(" ", $l - $lmsg1);
        $res .= ":" . $msg2;
        return $res;
    }
    function addLeftRight($msg1, $msg2, $l = CHARLENGTHRECEIPT)
    {
        $lmsg1 = strlen($msg1);
        $lmsg2 = strlen($msg2);
        $sisa = $l - ($lmsg1 + $lmsg2);
        return $msg1 . self::fillWithChar(" ", $sisa) . $msg2;
    }
    function addItemCodeReceipt($itemKode, $qty, $subtotal, $l = CHARLENGTHRECEIPT)
    {
        $litemKode = strlen($itemKode); // max 39
        $mitemKode = 39;
        $lqty = strlen($qty); //max 7
        $mqty = 7;
        $lsubtotal = strlen($subtotal); //max 17
        $msubtotal = 17;
        if ($litemKode > $mitemKode) {
            $itemKode = substr($itemKode, 0, $mitemKode);
//            $litemKode = $mitemKode;
        }
        if ($lqty > $mqty) {
            $qty = substr($qty, 0, $mitemKode);
//            $lqty = $mqty;
        }
        if ($lsubtotal > $msubtotal) {
            $subtotal = substr($subtotal, 0, $msubtotal);
//            $lsubtotal = $msubtotal;
        }
        $msg1 = self::addLeftRight($itemKode, $qty, $mitemKode + $mqty);
        return self::addLeftRight($msg1, $subtotal, $l);
    }
    function addItemNameReceipt($itemName, $l, $prefix = 5)
    {
        $itemName = self::fillWithChar(" ", $prefix) . $itemName;
        if (strlen($itemName) > $l) {
            return substr($itemName, 0, $l);
        }
        return $itemName;
    }
    function addItemDiscReceipt($disc, $subtotal, $prefix = 6, $l = CHARLENGTHRECEIPT)
    {
        $lbldisc = self::fillWithChar(" ", $prefix) . "Disc: $disc %";
        return self::addLeftRight($lbldisc, $subtotal, $l);
    }
} 