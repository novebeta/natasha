<?php
class MenuTree
{
    var $security_role;
    var $menu_users = "{text: 'User Manajement',
                                id: 'jun.UsersGrid',
                                leaf: true
                                },";
    var $security = "{text: 'Security Roles',
                                id: 'jun.SecurityRolesGrid',
                                leaf: true
                                },";
    function __construct($id)
    {
        $role = SecurityRoles::model()->findByPk($id);
        $this->security_role = explode(",", $role->sections);
    }
    function getChildMaster()
    {
        $child = "";
        $child .= in_array(100, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Country',
                            id: 'jun.NegaraGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(101, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Province',
                            id: 'jun.ProvinsiGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(102, $this->security_role) && !Users::is_audit() ? "{
                            text: 'City',
                            id: 'jun.KotaGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(103, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Sub District',
                            id: 'jun.KecamatanGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(104, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Doctor',
                            id: 'jun.DokterGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(105, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Bank',
                            id: 'jun.BankGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(106, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Group',
                            id: 'jun.GrupGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(107, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Items',
                            id: 'jun.BarangGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(108, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Customers',
                            id: 'jun.CustomersGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(109, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Beautician',
                            id: 'jun.BeautyGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(110, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Beautician Class',
                            id: 'jun.GolGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(111, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Beautician Tip Rate',
                            id: 'jun.PriceGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(112, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Reference',
                            id: 'jun.SysTypesGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(113, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Status Customers',
                            id: 'jun.StatusCustGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(114, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Discount',
                            id: 'jun.DiskonGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(115, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Store',
                            id: 'jun.StoreGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(116, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Supplier',
                            id: 'jun.SupplierGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(117, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Chart Of Account',
                            id: 'jun.ChartMasterGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(118, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Default Purchase Price',
                            id: 'jun.BeliGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(118, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Region',
                            id: 'jun.WilayahGrid',
                            leaf: true
                        }" : '';
        return $child;
    }
    function getMaster($child)
    {
        if ($child == null)
            return "";
        return "{
                    text: 'Master',
                    expanded: false,
                    children:[
                    $child
                    ]
                },";
    }
    function getTransaction($child)
    {
        if ($child == null)
            return "";
        return "{
                    text: 'Transaction',
                    expanded: false,
                    children:[
                    $child
                    ]
                },";
    }
    function getChildTransaction()
    {
        $child = "";
        $sales = Users::is_audit() ? "{
                            text: 'Sales',
                            id: 'jun.AuditGrid',
                            leaf: true
                    }," : "{
                            text: 'Sales',
                            id: 'jun.SalestransGrid',
                            leaf: true
                        },";
        $child .= in_array(200, $this->security_role) ? $sales : '';
        $child .= in_array(201, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Return Sales',
                            id: 'jun.ReturSalestransgrid',
                            leaf: true
                    }," : '';
        $child .= in_array(202, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Service',
                            id: 'jun.beautytransGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(203, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Cash/Bank Deposit',
                            id: 'jun.KasGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(204, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Cash/Bank Payment',
                            id: 'jun.KasGridOut',
                            leaf: true
                    }," : '';
        $child .= in_array(205, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Purchase Item',
                            id: 'jun.TransferItemGrid',
                            leaf: true
                    }," : '';
        $child .= in_array(206, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Return Supplier Item',
                            id: 'jun.ReturnTransferItemGrid',
                            leaf: true
                    }," : '';
        $child .= in_array(207, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Tender Declaration',
                            id: 'jun.TenderGrid',
                            leaf: true
                    }," : '';
        $child .= in_array(208, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Audit',
                            id: 'jun.AuditGrid',
                            leaf: true
                    }," : '';
        $child .= in_array(210, $this->security_role) && !Users::is_audit() ? "{
                            text: 'History',
                            id: 'jun.HistoryGrid',
                            leaf: true
                    }," : '';
        $child .= in_array(211, $this->security_role) && !Users::is_audit() ? "{
                            text: 'General Journal',
                            id: 'jun.GlTransWin',
                            leaf: true
                    }," : '';
        $child .= in_array(212, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Cash/Bank HO Deposit',
                            id: 'jun.KasGridPusat',
                            leaf: true
                    }," : '';
        $child .= in_array(213, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Cash/Bank HO Payment',
                            id: 'jun.KasGridPusatOut',
                            leaf: true
                    }," : '';
        $child .= in_array(214, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Cash/Bank Transfer',
                            id: 'jun.TranferBankWin',
                            leaf: true
                    }," : '';
        return $child;
    }
    function getReport($child)
    {
        if ($child == null)
            return "";
        return "{
                    text: 'Report',
                    expanded: false,
                    children:[
                    $child
                    ]
                },";
    }
    function getChildReport()
    {
        $child = "";
        $child .= in_array(313, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Sales',
                            id: 'jun.ReportSalesSummaryReceipt',
                            leaf: true
                        }," : '';
        $child .= in_array(313, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Sales Details',
                            id: 'jun.ReportSalesSummaryReceiptDetails',
                            leaf: true
                    }," : '';
        $child .= in_array(300, $this->security_role) ? "{
                            text: 'Sales Summary Group',
                            id: 'jun.ReportSalesSummary',
                            leaf: true
                        }," : '';
        $child .= in_array(301, $this->security_role) ? "{
                            text: 'Sales Details Group',
                            id: 'jun.ReportSalesDetails',
                            leaf: true
                        }," : '';
        $child .= in_array(302, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Inventory Movements',
                            id: 'jun.ReportInventoryMovements',
                            leaf: true
                        }," : '';
        $child .= in_array(303, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Inventory Card',
                            id: 'jun.ReportInventoryCard',
                            leaf: true
                    }," : '';
        $child .= in_array(304, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Beautician Services Summary',
                            id: 'jun.ReportBeautySummary',
                            leaf: true
                        }," : '';
        $child .= in_array(305, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Beautician Services Details',
                            id: 'jun.ReportBeautyDetails',
                            leaf: true
                        }," : '';
        $child .= in_array(311, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Doctors Services Summary',
                            id: 'jun.ReportDokterSummary',
                            leaf: true
                        }," : '';
        $child .= in_array(312, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Doctors Services Details',
                            id: 'jun.ReportDokterDetails',
                            leaf: true
                        }," : '';
        $child .= in_array(306, $this->security_role) && !Users::is_audit() ? "{
                            text: 'New Customers',
                            id: 'jun.ReportNewCustomers',
                            leaf: true
                        }," : '';
        $child .= in_array(307, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Tenders',
                            id: 'jun.ReportTender',
                            leaf: true
                    }," : '';
        $child .= in_array(308, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Daily Report',
                            id: 'jun.ReportLaha',
                            leaf: true
                    }," : '';
        $child .= in_array(309, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Return Sales Summary',
                            id: 'jun.ReportReturSalesSummary',
                            leaf: true
                    }," : '';
        $child .= in_array(310, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Return Sales Details',
                            id: 'jun.ReportReturSalesDetails',
                            leaf: true
                    }," : '';
        $child .= in_array(314, $this->security_role) && !Users::is_audit() ? "{
                            text: 'General Ledger',
                            id: 'jun.ReportGeneralLedger',
                            leaf: true
                    }," : '';
        $child .= in_array(315, $this->security_role) && !Users::is_audit() ? "{
                            text: 'General Journal',
                            id: 'jun.ReportGeneralJournal',
                            leaf: true
                    }," : '';
        return $child;
    }
    function getAdministration($child)
    {
        if ($child == null)
            return "";
        return "{
                    text: 'Administration',
                    expanded: false,
                    children:[
                    $child
                    ]
                },";
    }
    function getChildAdministration()
    {
        $child = "";
        $child .= in_array(400, $this->security_role) && !Users::is_audit() ? "{
                            text: 'User Management',
                            id: 'jun.UsersGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(401, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Security Roles',
                            id: 'jun.SecurityRolesGrid',
                            leaf: true
                        }," : '';
        $child .= in_array(402, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Backup / Restore',
                            id: 'jun.BackupRestoreWin',
                            leaf: true
                        }," : '';
        $child .= in_array(403, $this->security_role) && !Users::is_audit() ? "{
                            text: 'Import',
                            id: 'jun.ImportXlsx',
                            leaf: true
                        }," : '';
        return $child;
    }
    function getGeneral()
    {
        $username = Yii::app()->user->name;
        $child = "";
        $child .= in_array(000, $this->security_role) ? "{
                            text: 'Change Password',
                            id: 'jun.PasswordWin',
                            leaf: true
                        }," : '';
        $child .= in_array(001, $this->security_role) ? "{
                            text: 'Logout ($username)',
                            id: 'logout',
                            leaf: true
                        }," : '';
        return $child;
    }
    public function get_menu()
    {
        $username = Yii::app()->user->name;
        $data = "[";
        $data .= self::getMaster(self::getChildMaster());
        $data .= self::getTransaction(self::getChildTransaction());
        $data .= self::getReport(self::getChildReport());
        $data .= self::getAdministration(self::getChildAdministration());
        $data .= self::getGeneral();
        $data .= "]";
        return $data;
    }
}
