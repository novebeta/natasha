<?php
class SyncData
{
    private $result;
    private $username;
    private $password;
    private $status_error;
    private $skip_primary;
    public function sync()
    {
        $this->skip_primary = array('Diskon', 'Price', 'Payment', 'StockMoves',
            'BankTrans', 'SysPrefs', 'TenderDetails', 'TransferItemDetails', 'Beli',
            'GlTrans', 'Comments', 'BeautyServices');
        $this->username = yiiparam('Username');
        $this->password = yiiparam('Password');
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand("SET FOREIGN_KEY_CHECKS = 0")
                ->execute();
            self::upload_cust_nars();
//            self::upload_trans_nars();
//            self::download_global();
//            self::upload_global();
//            self::download_master();
//            if (BROADCAST) {
//                self::broadcast_master();
//            }
//            if (TOSTOREHO) {
//                self::upload_master_all();
//            }
//            self::download_transaksi();
//            self::upload_transaksi();
            Yii::app()->db->createCommand("SET FOREIGN_KEY_CHECKS = 1")
                ->execute();
            $msg = $this->result;
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            $msg = $ex->getMessage();
        }
        app()->db->autoCommit = true;
        return $msg;
    }
    private function upload_global()
    {
        $global = array('SecurityRoles', 'Gol', 'TransTipe',
            'StatusCust', 'ChartMaster', 'Supplier', 'Kategori', 'Negara',
            'Provinsi', 'Kota', 'Kecamatan', 'Store'
        );
        $criteria = new CDbCriteria;
        $criteria->addCondition("up = 0");
        $upload = array();
        $content = array();
        foreach ($global as $modelName) {
            $tables = CActiveRecord::model($modelName)->findAll($criteria);
            if ($tables != null) {
                $upload[$modelName] = $tables;
                $content[$modelName] = number_format(count($tables)) . ' record';
            }
        }
        if (count($upload) == 0) {
            $this->result['upload_master'] = 'nothing to upload';
            return;
        }
        $to = yiiparam('Username');
        $subject = STOREGLOBAL . '-' . sha1(STOREID . "^" . date('Y-m-d H:i:s'));
        $status = mailsend($to, $to, $subject,
            json_encode(array(
                'from' => STOREID,
                'date' => date('Y-m-d H:i:s'),
                'content' => array($content)
            ), JSON_PRETTY_PRINT),
            bzcompress(Encrypt(CJSON::encode($upload)), 9)
        );
        if ($status == "OK") {
            $succ = array();
            foreach ($upload as $k => $v) {
                $table = CActiveRecord::model($k)->tableName();
                $r = Yii::app()->db->createCommand("
                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                $succ[$k] = 'success upload ' . number_format($r) . ' records';
            }
            $this->result['upload_global'] = $succ;
            $sync = new Sync;
            $sync->type_ = "G";
            $sync->subject = $subject;
            $sync->save();
            return;
        }
        $this->result['upload_global'] = 'failed upload records';
    }
    private function upload_master_cabang($from = STOREID, $target = STOREIDTARGET)
    {
        $master = array('SysPrefs', 'Users', 'Bank', 'Barang', 'Customers',
            'Grup', 'Diskon', 'Price', 'Dokter', 'Beauty'
        );
        $criteria = new CDbCriteria;
        $criteria->addCondition("up = 0");
        $criteria->addCondition("store = :store");
        $criteria->params = array(':store' => $from);
        $upload = array();
        $content = array();
        foreach ($master as $modelName) {
            $tables = CActiveRecord::model($modelName)->findAll($criteria);
            if ($tables != null) {
                $upload[$modelName] = $tables;
                $content[$modelName] = number_format(count($tables)) . ' record';
            }
        }
        $to = yiiparam('Username');
        $subject = 'M' . $target . '-' . sha1($from . "^" . date('Y-m-d H:i:s'));
        $status = mailsend($to, $to, $subject,
            json_encode(array(
                'from' => $from,
                'date' => date('Y-m-d H:i:s'),
                'content' => array($content)
            ), JSON_PRETTY_PRINT),
            bzcompress(Encrypt(CJSON::encode($upload)), 9)
        );
        if ($status == "OK") {
            $succ = array();
            foreach ($upload as $k => $v) {
                $table = CActiveRecord::model($k)->tableName();
                $r = Yii::app()->db->createCommand("
                UPDATE $table SET up = 1 WHERE up = 0 AND store = :store")->execute(array(':store' => $from));
                $succ[$k] = 'success upload ' . number_format($r) . ' records';
            }
            $this->result['upload_master'][$target] = $succ;
            return;
        }
        $this->result['upload_master'][$target] = 'failed upload records';
    }
    private function upload_master_all($from = STOREID, $target = STOREIDTARGET)
    {
        $master = array('SysPrefs', 'Users', 'Bank', 'Barang', 'Customers',
            'Grup', 'Diskon', 'Price', 'Dokter', 'Beauty'
        );
        $criteria = new CDbCriteria;
        $criteria->addCondition("up = 0");
        $upload = array();
        $content = array();
        foreach ($master as $modelName) {
            $tables = CActiveRecord::model($modelName)->findAll($criteria);
            if ($tables != null) {
                $upload[$modelName] = $tables;
                $content[$modelName] = number_format(count($tables)) . ' record';
            }
        }
        $to = yiiparam('Username');
        $subject = 'M' . $target . '-' . sha1($from . "^" . date('Y-m-d H:i:s'));
        $status = mailsend($to, $to, $subject,
            json_encode(array(
                'from' => $from,
                'date' => date('Y-m-d H:i:s'),
                'content' => array($content)
            ), JSON_PRETTY_PRINT),
            bzcompress(Encrypt(CJSON::encode($upload)), 9)
        );
        if ($status == "OK") {
            $succ = array();
            foreach ($upload as $k => $v) {
                $table = CActiveRecord::model($k)->tableName();
                $r = Yii::app()->db->createCommand("
                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                $succ[$k] = 'success upload ' . number_format($r) . ' records';
            }
            $this->result['upload_master'][$target] = $succ;
            return;
        }
        $this->result['upload_master'][$target] = 'failed upload records';
    }
    private function broadcast_master()
    {
        $store = Store::model()->findAll('store_kode <> :store_kode',
            array(':store_kode' => STOREID));
        foreach ($store as $target) {
            self::upload_master_cabang(STOREID, $target->store_kode);
        }
    }
    private function upload_transaksi()
    {
        $trans = array('Refs', 'Comments', 'GlTrans',
            'StockMoves', 'BankTrans'
        );
        $criteria = new CDbCriteria;
        $criteria->addCondition("up = 0");
        $upload = array();
        $content = array();
        $trans_update = array();
        foreach ($trans as $modelName) {
            $tables = CActiveRecord::model($modelName)->findAll($criteria);
            if ($tables != null) {
                $upload[$modelName] = $tables;
                $trans_update[] = $modelName;
                $content[$modelName] = number_format(count($tables)) . ' record';
            }
        }
// Tender ==============================================================================================================
        $tender = Tender::model()->findAll($criteria);
//        $tender = new Tender;
        if ($tender != null) {
            $upload['Tender'] = $tender;
            $content['Tender'] = number_format(count($tender)) . ' record';
            $tenderDetails = array();
            foreach ($tender as $row) {
                if ($row->tenderDetails != null) {
                    $tenderDetails = array_merge($tenderDetails, $row->tenderDetails);
                }
            }
            if (count($tenderDetails) > 0) {
                $upload['TenderDetails'] = $tenderDetails;
                $content['TenderDetails'] = number_format(count($tenderDetails)) . ' record';
            }
            $trans_update[] = "Tender";
        }
// Tender ==============================================================================================================
// TransferItem ========================================================================================================
        $transferItem = TransferItem::model()->findAll($criteria);
//        $tender = new TransferItem;
        if ($transferItem != null) {
            $upload['TransferItem'] = $transferItem;
            $content['TransferItem'] = number_format(count($transferItem)) . ' record';
            $transferItemDetails = array();
            foreach ($transferItem as $row) {
                if ($row->transferItemDetails != null) {
                    $transferItemDetails = array_merge($transferItemDetails, $row->transferItemDetails);
                }
            }
            if (count($transferItemDetails) > 0) {
                $upload['TransferItemDetails'] = $transferItemDetails;
                $content['TransferItemDetails'] = number_format(count($transferItemDetails)) . ' record';
            }
            $trans_update[] = "TransferItem";
        }
// TransferItem ========================================================================================================
// Kas =================================================================================================================
        $kas = Kas::model()->findAll($criteria);
//        $kas = new Kas;
        if ($kas != null) {
            $upload['Kas'] = $kas;
            $content['Kas'] = number_format(count($kas)) . ' record';
            $kasDetails = array();
            foreach ($kas as $row) {
                if ($row->kasDetails != null) {
                    $kasDetails = array_merge($kasDetails, $row->kasDetails);
                }
            }
            if (count($kasDetails) > 0) {
                $upload['KasDetail'] = $kasDetails;
                $content['KasDetail'] = number_format(count($kasDetails)) . ' record';
            }
            $trans_update[] = "Kas";
        }
// Kas =================================================================================================================
// Salestrans ==========================================================================================================
        $salestrans = Salestrans::model()->findAll($criteria);
        if ($salestrans != null) {
            $upload['Salestrans'] = $salestrans;
            $content['Salestrans'] = number_format(count($salestrans)) . ' record';
            $salestransDetails = array();
            $payments = array();
            foreach ($salestrans as $row) {
                if ($row->salestransDetails != null) {
                    $salestransDetails = array_merge($salestransDetails, $row->salestransDetails);
                }
                if ($row->payments != null) {
                    $payments = array_merge($payments, $row->payments);
                }
            }
            if (count($salestransDetails) > 0) {
                $upload['SalestransDetails'] = $salestransDetails;
                $content['SalestransDetails'] = number_format(count($salestransDetails)) . ' record';
            }
            if (count($payments) > 0) {
                $upload['Payments'] = $payments;
                $content['Payments'] = number_format(count($payments)) . ' record';
            }
            $trans_update[] = "Salestrans";
        }
// Salestrans ==========================================================================================================
// BeautyServices ======================================================================================================
        $beautyServices = BeautyServices::model()->findAll($criteria);
        if ($beautyServices != null) {
            $upload['BeautyServices'] = $beautyServices;
            $content['BeautyServices'] = number_format(count($beautyServices)) . ' record';
            $trans_update[] = "BeautyServices";
        }
// BeautyServices ======================================================================================================
        if (count($upload) == 0) {
            $this->result['upload_transaksi'] = 'nothing to upload';
            return;
        }
        $to = yiiparam('Username');
        $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . date('Y-m-d H:i:s'));
        $status = mailsend($to, $to, $subject,
            json_encode(array(
                'from' => STOREID,
                'date' => date('Y-m-d H:i:s'),
                'content' => array($content)
            ), JSON_PRETTY_PRINT),
            bzcompress(Encrypt(CJSON::encode($upload)), 9)
        );
        if ($status == "OK") {
            $succ = array();
            foreach ($trans_update as $k) {
                $table = CActiveRecord::model($k)->tableName();
                $r = Yii::app()->db->createCommand("
                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                $succ[$k] = 'success upload ' . number_format($r) . ' records';
            }
            $this->result['upload_transaksi'] = $succ;
            return;
        }
        $this->result['upload_transaksi'] = 'failed upload records';
    }
    private function download_global()
    {
        $json = self::download_email(STOREGLOBAL, 'G');
        if ($json === false) {
            $this->result['download_global'] = $this->status_error;
            return;
        }
        foreach ($json as $email) {
            $data = json_decode($email, true);
            foreach ($data as $k => $v) {
                foreach ($v as $row) {
                    $primarykey = CActiveRecord::model($k)->tableSchema->primaryKey;
                    $pkval = $row[$primarykey];
                    $model = CActiveRecord::model($k)->findByPk($pkval);
                    if ($model == null) {
                        $model = new $k;
                    }
                    $row['up'] = 1;
                    $model->attributes = $row;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => $k)) . CHtml::errorSummary($model));
                    }
                }
                $this->result['download_global'][$k] = 'success download ' . number_format($v) . ' records';
            }
        }
    }
    private function download_master()
    {
        $json = self::download_email(STOREID, 'M');
        if ($json === false) {
            $this->result['download_master'] = $this->status_error;
            return;
        }
        foreach ($json as $email) {
            $data = json_decode($email, true);
            foreach ($data as $k => $v) {
                foreach ($v as $row) {
                    $primarykey = CActiveRecord::model($k)->tableSchema->primaryKey;
                    if (in_array($k, $this->skip_primary)) {
                        $model = null;
                        $row[$primarykey] = null;
                    } else {
                        $pkval = $row[$primarykey];
                        $model = CActiveRecord::model($k)->findByPk($pkval);
                    }
                    if ($model == null) {
                        $model = new $k;
                    }
                    $row['up'] = MARKAFTERDOWNLOAD ? 1 : 0;
                    $model->attributes = $row;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => $k)) . CHtml::errorSummary($model));
                    }
                }
                $this->result['download_master'][$k] = 'success download ' . number_format($v) . ' records';
            }
        }
    }
    private function download_transaksi()
    {
        $remove_beautyservices = array();
        $json = self::download_email(STOREID, 'T');
        if ($json === false) {
            $this->result['download_master'] = $this->status_error;
            return;
        }
        foreach ($json as $email) {
            $data = json_decode($email, true);
            foreach ($data as $k => $v) {
                foreach ($v as $row) {
                    if ($k == 'BeautyServices') {
                        if (!in_array($row['salestrans_details'], $remove_beautyservices)) {
                            $delete = Yii::app()->db->createCommand("
                            DELETE FROM nscc_beauty_services WHERE salestrans_details = :salestrans_details");
                            $delete->execute(array(':salestrans_details' => $row['salestrans_details']));
                            $remove_beautyservices[] = $row['salestrans_details'];
                        }
                    }
                    $primarykey = CActiveRecord::model($k)->tableSchema->primaryKey;
                    if (in_array($k, $this->skip_primary)) {
                        $model = null;
                        $row[$primarykey] = null;
                    } else {
                        $pkval = $row[$primarykey];
                        $model = CActiveRecord::model($k)->findByPk($pkval);
                    }
                    if ($model == null) {
                        $model = new $k;
                    }
                    $row['up'] = 1;
                    $model->attributes = $row;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => $k)) . CHtml::errorSummary($model));
                    }
                }
                $this->result['download_master'][$k] = 'success download ' . number_format($v) . ' records';
            }
        }
    }
    private function upload_cust_nars()
    {
        $cust = Customers::get_backup();
        if (count($cust) == 0) {
            $this->result['upload_cust_nars'] = 'nothing to upload';
            return;
        }
        $url = SysPrefs::get_val('url_nars');
        $data = array(
            'username' => SysPrefs::get_val('username_nars'),
            'password' => SysPrefs::get_val('password_nars'),
            'cabang' => STOREID,
            'tanggal' => "",
            'jenis' => 'pasien',
            'data' => CJSON::encode($cust)
        );
        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($data),
            ),
        );
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result == '1') {
            $r = Yii::app()->db->createCommand("
           UPDATE nscc_customers SET log = 1 WHERE log = 0
           ")->execute();
            $this->result['upload_cust_nars'] = 'success upload ' . number_format($r) . ' records';
            return;
        }
        $this->result['upload_cust_nars'] = 'failed upload records';
    }
    private function upload_trans_nars()
    {
        $sales = Salestrans::get_backup();
        if (count($sales) == 0) {
            $this->result['upload_trans_nars'] = 'nothing to upload';
            return;
        }
        $url = SysPrefs::get_val('url_nars');
        $data = array(
            'username' => SysPrefs::get_val('username_nars'),
            'password' => SysPrefs::get_val('password_nars'),
            'cabang' => STOREID,
            'tanggal' => "",
            'jenis' => 'transaksi',
            'data' => CJSON::encode($sales)
        );
        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($data),
            ),
        );
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result == '1') {
            $r = Yii::app()->db->createCommand("
           UPDATE nscc_salestrans SET log = 1 WHERE log = 0
           ")->execute();
            $this->result['upload_trans_nars'] = 'success upload ' . number_format($r) . ' records';
            return;
        }
        $this->result['upload_trans_nars'] = 'failed upload records';
    }
    private function download_email($imapmainbox, $type_)
    {
        $json = array();
        $messagestatus = "ALL";
        $imapaddress = "{imap.gmail.com:993/imap/ssl}";
        $hostname = $imapaddress . $imapmainbox;
        $connection = @imap_open($hostname, $this->username, $this->password, OP_READONLY);
        if ($connection === false) {
            $this->status_error = 'Cannot connect to Gmail: ' . imap_last_error();
            return false;
        }
        $emails = imap_search($connection, $messagestatus);
        if ($emails) {
            foreach ($emails as $email_number) {
                $header = imap_fetch_overview($connection, $email_number, 0);
                $subject = $header[0]->subject;
                $criteria = new CDbCriteria;
                $criteria->addCondition("type_ = :type_ AND subject = :subject");
                $criteria->params = array(':type_' => $type_, ':subject' => $subject);
                $count = Sync::model()->count($criteria);
                if ($count > 0) {
                    continue;
                }
                $message = imap_fetchbody($connection, $email_number, 1.1);
                if ($message == "") {
                    $message = imap_fetchbody($connection, $email_number, 1);
                }
                $structure = imap_fetchstructure($connection, $email_number);
                $attachments = array();
                if (isset($structure->parts) && count($structure->parts)) {
                    for ($i = 0; $i < count($structure->parts); $i++) {
                        $attachments[$i] = array(
                            'is_attachment' => false,
                            'filename' => '',
                            'name' => '',
                            'attachment' => '');
                        if ($structure->parts[$i]->ifdparameters) {
                            foreach ($structure->parts[$i]->dparameters as $object) {
                                if (strtolower($object->attribute) == 'filename') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['filename'] = $object->value;
                                }
                            }
                        }
                        if ($structure->parts[$i]->ifparameters) {
                            foreach ($structure->parts[$i]->parameters as $object) {
                                if (strtolower($object->attribute) == 'name') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['name'] = $object->value;
                                }
                            }
                        }
                        if ($attachments[$i]['is_attachment']) {
                            $attachments[$i]['attachment'] = imap_fetchbody($connection, $email_number, $i + 1);
                            if ($structure->parts[$i]->encoding == 3) { // 3 = BASE64
                                $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                            } elseif ($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
                                $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                            }
                        }
                    }
                }
                if (count($attachments) != 0) {
                    foreach ($attachments as $at) {
                        if ($at['is_attachment'] == 1) {
                            $json[] = Decrypt(bzdecompress($at['attachment']));
                            $sync = new Sync;
                            $sync->type_ = "G";
                            $sync->subject = $subject;
                            $sync->message = $message;
                            $sync->save();
                        }
                    }
                }
            }
        }
        imap_close($connection);
        return $json;
    }
} 