<?php
class AuditDetailsController extends GxController
{
    public function actionCreate()
    {
        $model = new AuditDetails;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['AuditDetails'][$k] = $v;
            }
            $model->attributes = $_POST['AuditDetails'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->audit_details;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'AuditDetails');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['AuditDetails'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['AuditDetails'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->audit_details;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->audit_details));
            }
        }
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('audit_id = :audit_id');
        $criteria->params = array(":audit_id"=>$_POST['audit_id']);
        $model = AuditDetails::model()->findAll($criteria);
        $total = AuditDetails::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}