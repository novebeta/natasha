<?php
class BankController extends GxController
{
    public function actionCreate()
    {
        $model = new Bank;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $id = $_POST['account_code'];
            $status = true;
            //$msg = "Akun bank berhasil dibuat";
            $model = new Bank;
            if (U::account_in_gl_trans($id)) {
                $status = false;
                $msg = t('coa.fail.use.gl', 'app', array('{coa}' => $id));
            } elseif (U::account_used_bank($id)) {
                $status = false;
                $msg = t('coa.fail.use.model', 'app', array('{coa}' => $id, '{model}' => 'Bank'));
            }
            if (!$status) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            }
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Bank'][$k] = $v;
            }
            $model->attributes = $_POST['Bank'];
            $msg = t('save.fail', 'app');
            if ($model->save()) {
                $status = true;
                $msg = t('save.success.id', 'app', array('{id}' => $model->bank_id));
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Bank');
        if (isset($_POST) && !empty($_POST)) {
            $id = $_POST['account_code'];
            $status = true;
            //$msg = "Akun bank berhasil dibuat";
            if (U::account_in_gl_trans($id)) {
                $status = false;
                $msg = t('coa.fail.use.gl', 'app', array('{coa}' => $id));
            } elseif (U::account_used_bank($id)) {
                $status = false;
                $msg = t('coa.fail.use.model', 'app', array('{coa}' => $id, '{model}' => 'Bank'));
            }
            if (!$status) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            }
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Bank'][$k] = $v;
            }
            $msg = t('save.fail', 'app');
            $model->attributes = $_POST['Bank'];
            if ($model->save()) {
                $status = true;
                $msg = t('save.success.id', 'app', array('{id}' => $model->bank_id));
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->bank_id));
            }
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if (isset ($_POST['mode']) && $_POST['mode'] == 'bank_cabang') {
            $criteria->addCondition("bank_id <> :bank_id");
            $criteria->params = array(':bank_id' => SysPrefs::get_val('kas_pusat'));
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = Bank::model()->findAll($criteria);
        $total = Bank::model()->count($criteria);
        $this->renderJson($model, $total);
    }

}