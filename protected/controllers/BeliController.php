<?php
class BeliController extends GxController
{
    public function actionCreate()
    {
        $model = new Beli;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Beli'][$k] = $v;
            }
            $model->attributes = $_POST['Beli'];
            $msg = t('save.fail','app');
            $result = Beli::save_default_price($_POST['Beli']['barang_id'], $_POST['Beli']['price'],
                $_POST['Beli']['tax'],$_POST['Beli']['store']);
            if ($result > 0) {
                $status = true;
                $msg = t('save.success','app'); // . $model->price_id;
            } else {
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionCreateRegion()
    {
        $model = new Beli;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Beli'][$k] = $v;
            }
            $model->attributes = $_POST['Beli'];
            $msg = t('save.fail','app');
            $result = Beli::save_default_price_by_region($_POST['Beli']['barang_id'], $_POST['Beli']['price'],
                $_POST['Beli']['tax'],$_POST['Beli']['wilayah']);
            if ($result > 0) {
                $status = true;
                $msg = t('save.success','app'); // . $model->price_id;
            } else {
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = Beli::model()->findAll($criteria);
        $total = Beli::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}