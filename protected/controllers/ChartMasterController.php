<?php
class ChartMasterController extends GxController
{
    public function actionCreate()
    {
        $model = new ChartMaster;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['ChartMaster'][$k] = $v;
            }
            $model->attributes = $_POST['ChartMaster'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->account_code;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'ChartMaster');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['ChartMaster'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['ChartMaster'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->account_code;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->account_code));
            }
        }
    }
    public function actionIndex()
    {
//        if (isset($_POST['limit'])) {
//            $limit = $_POST['limit'];
//        } else {
//            $limit = 20;
//        }
//        if (isset($_POST['start'])) {
//            $start = $_POST['start'];
//        } else {
//            $start = 0;
//        }
        $criteria = new CDbCriteria();
        if (isset ($_POST['mode'])) {
            switch ($_POST['mode']) {
                case 'detil':
                    $criteria->addCondition("header = 0");
                    $criteria->addCondition("kategori <> :bank AND kategori <> :kas");
                    $criteria->params = array(
                        ':bank'=>SysPrefs::get_val('coa_grup_bank'),
                        ':kas'=>SysPrefs::get_val('coa_grup_kas'),
                    );
                    break;
                case 'header':
                    $criteria->addCondition("header = 1");
                    break;
                case 'biaya':
                    $c = ChartMaster::get_child(SysPrefs::get_val('coa_grup_biaya'));
                    $this->renderJson($c, count($c));
                    Yii::app()->end();
                    break;
                case 'pendapatan':
                    $c = ChartMaster::get_child(SysPrefs::get_val('coa_grup_pendapatan'));
                    $this->renderJson($c, count($c));
                    Yii::app()->end();
                    break;
                case 'bank':
                    $c = ChartMaster::get_child(SysPrefs::get_val('coa_grup_bank'));
                    $d = ChartMaster::get_child(SysPrefs::get_val('coa_grup_kas'));
                    $e = array_merge($c, $d);
                    $this->renderJson($e, count($e));
                    Yii::app()->end();
                    break;
                case 'hutang':
                    $c = ChartMaster::get_child(SysPrefs::get_val('coa_grup_hutang'));
                    $this->renderJson($c, count($c));
                    Yii::app()->end();
                    break;
            }
        }
        $model = ChartMaster::model()->findAll($criteria);
        $total = ChartMaster::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}