<?php
class GlTransController extends GxController
{
    public function actionCreate()
    {
        if (!app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $msg = t('save.success', 'app');
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = app()->db->beginTransaction();
            try {
                $ref = new Reference();
                $docref = $ref->get_next_reference(JURNAL_UMUM);
                $jurnal_umum_id = U::get_max_type_no(JURNAL_UMUM);
                $jurnal_umum_id++;
                foreach ($detils as $detil) {
                    $amount = $detil['debit'] > 0 ? $detil['debit'] : -$detil['kredit'];
                    U::add_gl(JURNAL_UMUM, $jurnal_umum_id, $_POST['tran_date'], $docref,
                        $detil['account_code'], $detil["memo_"], '', $amount, 0);
                }
                $ref->save(JURNAL_UMUM, $jurnal_umum_id, $docref);
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            app()->end();
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = GlTrans::model()->findAll($criteria);
        $total = GlTrans::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}