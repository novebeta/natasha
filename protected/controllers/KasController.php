<?php
Yii::import('application.components.Reference');
Yii::import('application.components.U');
Yii::import('application.components.PrintCashOut');
class KasController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            if (Tender::is_exist(date("Y-m-d"))) {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => 'Tender Declaration already created'));
                Yii::app()->end();
            }
            if (isset($_POST['modal'])) {
                $_POST['type_'] = 1;
                if (Kas::is_modal_exist(date("Y-m-d"))) {
                    echo CJSON::encode(array(
                        'success' => false,
                        'msg' => 'Equity Declaration already created'));
                    Yii::app()->end();
                }
            }
            $is_in = $_POST['arus'] == 1;
            $msg = "Data gagal disimpan.";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new Kas;
                $ref = new Reference();
                $docref = $ref->get_next_reference($is_in ? CASHIN : CASHOUT);
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil' || $k == 'arus') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Kas'][$k] = $v;
                }
                $_POST['Kas']['doc_ref'] = $docref;
                $_POST['Kas']['tgl'] = new CDbExpression('NOW()');
                $_POST['Kas']['total'] = $is_in ? $_POST['Kas']['total'] : -$_POST['Kas']['total'];
                $model->attributes = $_POST['Kas'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Cash')) . CHtml::errorSummary($model));
                U::add_gl($is_in ? CASHIN : CASHOUT, $model->kas_id, $model->tgl, $docref, $model->bank->account_code,
                    $is_in ? "Cash In" : "Cash Out", $is_in ? "Cash In" : "Cash Out", $model->total, 0);
                foreach ($detils as $detil) {
                    $kas_detail = new KasDetail;
                    $_POST['KasDetail']['account_code'] = $detil['account_code'];
                    $_POST['KasDetail']['item_name'] = $detil['item_name'];
                    $_POST['KasDetail']['qty'] = get_number($detil['qty']);
                    $_POST['KasDetail']['price'] = $is_in ? get_number($detil['price']) : -get_number($detil['price']);
                    $_POST['KasDetail']['total'] = $is_in ? get_number($detil['total']) : -get_number($detil['total']);
                    $_POST['KasDetail']['kas_id'] = $model->kas_id;
                    $kas_detail->attributes = $_POST['KasDetail'];
                    if (!$kas_detail->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Detail Cash')) . CHtml::errorSummary($kas_detail));
                    U::add_gl($is_in ? CASHIN : CASHOUT, $model->kas_id, $model->tgl, $docref, $kas_detail->account_code,
                        $is_in ? "Cash In" : "Cash Out", "", -$kas_detail->total, 1);
                }
                $ref->save($is_in ? CASHIN : CASHOUT, $model->kas_id, $docref);
                $msg = t('save.success', 'app');
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Kas');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Kas'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Kas'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->kas_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->kas_id));
            }
        }
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        if (isset ($_POST['mode'])) {
            $criteria->select = "kas_id,doc_ref,no_kwitansi,keperluan,IF(total>=0,total,-total) total,bank_id,who,tgl,user_id,tdate,type_,store";
            $criteria->addCondition($_POST['mode'] == 'masuk' ? "total >= 0" : "total < 0");
            $criteria->addCondition('DATE(tgl) = :tgl');
            $criteria->params = array(':tgl' => $_POST['tgl']);
        }
        $model = Kas::model()->findAll($criteria);
        $total = Kas::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionPrint()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $sls = Kas::model()->findByPk($_POST['id']);
            $prt = new PrintCashOut($sls);
            echo CJSON::encode(array(
                'success' => $sls != null,
                'msg' => $prt->buildTxt()));
            Yii::app()->end();
        }
    }
    public function actionCreateTransfer()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = 'Transfer has been entered.';
            $id = -1;
            $bank_asal = $_POST['bank_act_asal'];
            $bank_tujuan = $_POST['bank_act_tujuan'];
            $trans_date = $_POST['trans_date'];
            $memo = $_POST['memo'];
            $amount = get_number($_POST['amount']);
            $charge = get_number($_POST['charge']);
            Yii::import('application.components.U');
            Yii::import('application.components.Reference');
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $ref = new Reference();
                $docref = $ref->get_next_reference(BANKTRANSFER);
                $bank_account_asal = U::get_act_code_from_bank_act($bank_asal);
                $bank_account_tujuan = U::get_act_code_from_bank_act($bank_tujuan);
                $trans_no = U::get_next_trans_no_bank_trans();
                $user = Yii::app()->user->getId();
                //debet kode bank tujuan - kredit kode bank asal
                U::add_gl(BANKTRANSFER, $trans_no, $trans_date, $docref, $bank_account_tujuan,
                    '', $memo, $amount, 0);
                U::add_gl(BANKTRANSFER, $trans_no, $trans_date, $docref, $bank_account_asal,
                    '', $memo, -$amount, 0);
                if ($charge > 0) {
                    U::add_gl(BANKTRANSFER, $trans_no, $trans_date, $docref, SysPrefs::get_val('coa_biaya_adm_bank'),
                        'Transfer kas/bank charge', $memo, $charge, 1);
                    U::add_gl(BANKTRANSFER, $trans_no, $trans_date, $docref, $bank_account_asal,
                        'Transfer kas/bank charge', $memo, -$charge, 0);
                }
                $ref->save(BANKTRANSFER, $trans_no, $docref);
                $id = $docref;
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $id,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
}