<?php
Yii::import('application.components.U');
Yii::import("application.components.tbs_class_php5", true);
Yii::import("application.components.tbs_plugin_excel", true);
Yii::import("application.components.tbs_plugin_aggregate", true);
class ReportController extends GxController
{
    private $TBS;
    private $logo;
    private $format;
    public function init()
    {
        parent::init();
        if (!isset($_POST) || empty($_POST)) {
            $this->redirect(url('/'));
        }
        $this->logo = bu() . "/images/logo.png";
        $this->TBS = new clsTinyButStrong;
        $this->format = $_POST['format'];
        if ($this->format == 'excel') {
            $this->TBS->PlugIn(TBS_INSTALL, TBS_EXCEL);
        }
        error_reporting(E_ERROR);
    }
    public function actionViewCustomerHistory()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $customer_id = $_POST['customer_id'];
            $tgl = $_POST['tglfrom'];
            $cust = U::report_view_customer_history($customer_id, $tgl);
            $customer = Customers::model()->findByPk($customer_id);
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'newcustomers.xml');
            } else {
//                $this->renderJsonArr($cust);
//                Yii::app()->end();
                $dataProvider = new CArrayDataProvider($cust, array(
                    'id' => 'user',
                    'pagination' => false
                ));
                $this->render('ViewCustomerHistory', array('dp' => $dataProvider, 'start' => sql2date($tgl, 'dd MMM yyyy'), 'cust' => $customer));
                Yii::app()->end();
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'view_customer_history.htm');
            }
            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'customer' => $customer->nama_customer)));
            $this->TBS->MergeBlock('history', $cust);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "ViewCustomerHistory_$customer->nama_customer.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionTenders()
    {
        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $branch = STOREID;
            $tenders = U::report_tenders($from);
            $sum_system = array_sum(array_column($tenders, 'system'));
            $sum_tenders = array_sum(array_column($tenders, 'tenders'));
            $selisih = 0;
            $status = "";
            if ($sum_system >= $sum_tenders) {
                $selisih = $sum_system - $sum_tenders;
                $status = "Short";
            } elseif ($sum_system < $sum_tenders) {
                $selisih = $sum_tenders - $sum_system;
                $status = "Over";
            }
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'tenders.xml');
            } else {
                $dataProvider = new CArrayDataProvider($tenders, array(
                    'id' => 'Tenders',
                    'pagination' => false
                ));
                $this->render('Tenders', array('dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'tender' => $status, 'selisih' => $selisih
                ));
                Yii::app()->end();
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'tenders.htm');
            }
            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'branch' => $branch, 'start' => $from)));
            $this->TBS->MergeBlock('tender', $tenders);
            $this->TBS->MergeBlock('s', array(array('tender' => $status, 'selisih' => $selisih)));
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "Tenders_$from.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionNewCustomers()
    {
        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $cust = U::report_new_customers($from, $to);
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'newcustomers.xml');
            } else {
                $dataProvider = new CArrayDataProvider($cust, array(
                    'id' => 'InventoryMovements',
                    'pagination' => false
                ));
                $this->render('NewCustomers', array('dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ));
                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'newcustomers.htm');
            }
            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to)));
            $this->TBS->MergeBlock('cust', $cust);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "newcustomers$from-$to.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionCashOut()
    {
        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'biaya.xml');
            } else {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'biaya.htm');
            }
            $biaya = U::report_biaya($from, $to);
            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to)));
            $this->TBS->MergeBlock('cash', $biaya);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "cashout$from-$to.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionInventoryMovements()
    {
        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $mutasi = U::report_mutasi_stok($from, $to);
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'mutasi_stok.xml');
            } else {
                $dataProvider = new CArrayDataProvider($mutasi, array(
                    'id' => 'InventoryMovements',
                    'pagination' => false
                ));
                $this->render('InventoryMovements', array('dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                ));
                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'mutasi_stok.htm');
            }
            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to)));
            $this->TBS->MergeBlock('mutasi', $mutasi);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "InventoryMovements$from-$to.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionInventoryCard()
    {
        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $barang_id = $_POST['barang_id'];
            $saldo_awal = StockMoves::get_saldo_item_before($barang_id, $from);
            $barang = Barang::model()->findByPk($barang_id);
            $row = U::report_kartu_stok($barang_id, $from, $to);
            $stock_card = array();
            foreach ($row as $newrow) {
                $newrow['before'] = $saldo_awal;
                $newrow['after'] += $newrow['before'] + $newrow['qty'];
                $saldo_awal = $newrow['after'];
                $stock_card[] = $newrow;
            }
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'kartu_stok.xml');
            } else {
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'kartu_stok.htm');
                $dataProvider = new CArrayDataProvider($stock_card, array(
                    'id' => 'InventoryCard',
                    'pagination' => false
                ));
                $this->render('InventoryCard', array('dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'item' => $barang
                ));
                Yii::app()->end();
            }
            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to, 'item' => $barang->nama_barang)));
            $this->TBS->MergeBlock('mutasi', $stock_card);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "InventoryCard$from-$to.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionBeautySummary()
    {
        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $summary = U::report_beauty_summary($from, $to);
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'beauty_rekap.xml');
            } else {
                $dataProvider = new CArrayDataProvider($summary, array(
                    'id' => 'BeautySummary',
                    'pagination' => false
                ));
                $this->render('BeautySummary', array('dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'), 'to' => sql2date($to, 'dd MMM yyyy')));
                Yii::app()->end();
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'beauty_rekap.htm');
            }
            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to)));
            $this->TBS->MergeBlock('beauty', $summary);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "BeautySummary$from-$to.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionDokterSummary()
    {
        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $summary = U::report_dokter_summary($from, $to);
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'dokter_rekap.xml');
            } else {
                $dataProvider = new CArrayDataProvider($summary, array(
                    'id' => 'DokterSummary',
                    'pagination' => false
                ));
                $this->render('DokterSummary', array('dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'), 'to' => sql2date($to, 'dd MMM yyyy')));
                Yii::app()->end();
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'dokter_rekap.htm');
            }
            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to)));
            $this->TBS->MergeBlock('dokter', $summary);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "DokterSummary$from-$to.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionDokterDetails()
    {
        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $summary = U::report_dokter_details($from, $to);
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'dokter_details.xml');
            } else {
                $dataProvider = new CArrayDataProvider($summary, array(
                    'id' => 'DokterDetails',
                    'pagination' => false
                ));
                $this->render('DokterDetails', array('dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'), 'to' => sql2date($to, 'dd MMM yyyy')));
                Yii::app()->end();
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'dokter_details.htm');
            }
            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to)));
            $this->TBS->MergeBlock('dokter', $summary);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "DokterDetails$from-$to.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionBeautyDetails()
    {
        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $summary = U::report_beauty_details($from, $to);
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'beauty_details.xml');
            } else {
                $dataProvider = new CArrayDataProvider($summary, array(
                    'id' => 'BeautyDetails',
                    'pagination' => false
                ));
                $this->render('BeautyDetails', array('dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'), 'to' => sql2date($to, 'dd MMM yyyy')));
                Yii::app()->end();
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'beauty_details.htm');
            }
            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to)));
            $this->TBS->MergeBlock('beauty', $summary);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "BeautyDetails$from-$to.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionSalesSummary()
    {
        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $grup_id = $_POST['grup_id'];
            $grup_name = 'ALL';
            if ($grup_id != null) {
                $grup = Grup::model()->findByPk($grup_id);
                $grup_name = $grup->nama_grup;
            }
            $summary = Users::is_audit() ? U::report_audit_summary($grup_id, $from, $to) : U::report_sales_summary($grup_id, $from, $to);
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales_rekap.xml');
            } else {
                $dataProvider = new CArrayDataProvider($summary, array(
                    'id' => 'SalesSummary',
                    'pagination' => false
                ));
                $this->render('SalesSummary', array('dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'), 'to' => sql2date($to, 'dd MMM yyyy'), 'grup' => $grup_name));
                Yii::app()->end();
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales_rekap.htm');
            }
            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to, 'grup' => $grup_name)));
            $this->TBS->MergeBlock('sales', $summary);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SalesSummary$from-$to.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionSalesSummaryReceipt()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $summary = U::report_sales_summary_receipt($from, $to);
//            $total = array_sum(array_column($summary, 'total'));
            $amount = array_sum(array_column($summary, 'amount'));
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.xml');
            } else {
                $dataProvider = new CArrayDataProvider($summary, array(
                    'id' => 'user',
                    'pagination' => false
                ));
                $this->render('SalesSummaryReceipt',
                    array('dp' => $dataProvider, 'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'), 'total' => $amount, 'amount' => $amount));
//                $this->renderJsonArr($summary);
                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.htm');
            }
            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from,)));
            $this->TBS->MergeBlock('sales', $summary);
            $this->TBS->MergeBlock('s', array(array('total' => $amount, 'amount' => $amount)));
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SalesSummaryReceipt$from.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionSalesSummaryReceiptDetails()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $summary = U::report_sales_summary_receipt_details($from, $to);
            $total = array_sum(array_column($summary, 'total'));
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.xml');
            } else {
                $dataProvider = new CArrayDataProvider($summary, array(
                    'id' => 'user',
                    'pagination' => false
                ));
                $this->render('SalesSummaryReceiptDetails',
                    array('dp' => $dataProvider, 'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'), 'total' => $total));
//                $this->renderJsonArr($summary);
                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.htm');
            }
            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from,)));
            $this->TBS->MergeBlock('sales', $summary);
            $this->TBS->MergeBlock('s', array(array('total' => $total)));
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SalesSummaryReceiptDetails$from.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionReturSalesSummary()
    {
        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $grup_id = $_POST['grup_id'];
            $grup_name = 'ALL';
            if ($grup_id != null) {
                $grup = Grup::model()->findByPk($grup_id);
                $grup_name = $grup->nama_grup;
            }
            $summary = U::report_retursales_summary($grup_id, $from, $to);
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'retursales_rekap.xml');
            } else {
                $dataProvider = new CArrayDataProvider($summary, array(
                    'id' => 'ReturSalesSummary',
                    'pagination' => false
                ));
                $this->render('ReturSalesSummary', array('dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'), 'to' => sql2date($to, 'dd MMM yyyy'), 'grup' => $grup_name));
                Yii::app()->end();
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'retursales_rekap.htm');
            }
            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to, 'grup' => $grup_name)));
            $this->TBS->MergeBlock('sales', $summary);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SalesSummary$from-$to.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionSalesDetails()
    {
        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $grup_id = $_POST['grup_id'];
            $grup_name = 'ALL';
            if ($grup_id != null) {
                $grup = Grup::model()->findByPk($grup_id);
                $grup_name = $grup->nama_grup;
            }
            $summary = Users::is_audit() ? U::report_audit_details($grup_id, $from, $to) : U::report_sales_details($grup_id, $from, $to);
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales_details.xml');
            } else {
                $dataProvider = new CArrayDataProvider($summary, array(
                    'id' => 'SalesDetails',
                    'pagination' => false
                ));
                $this->render('SalesDetails', array('dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'), 'to' => sql2date($to, 'dd MMM yyyy'), 'grup' => $grup_name));
                Yii::app()->end();
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales_details.htm');
            }
            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => sql2date($to, 'dd MMM yyyy'), 'grup' => $grup_name)));
            $this->TBS->MergeBlock('sales', $summary);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SalesDetails$from-$to.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionReturSalesDetails()
    {
        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $grup_id = $_POST['grup_id'];
            $grup_name = 'ALL';
            if ($grup_id != null) {
                $grup = Grup::model()->findByPk($grup_id);
                $grup_name = $grup->nama_grup;
            }
            $summary = U::report_retur_sales_details($grup_id, $from, $to);
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales_details.xml');
            } else {
                $dataProvider = new CArrayDataProvider($summary, array(
                    'id' => 'ReturSalesDetails',
                    'pagination' => false
                ));
                $this->render('ReturSalesDetails', array('dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'), 'to' => sql2date($to, 'dd MMM yyyy'), 'grup' => $grup_name));
                Yii::app()->end();
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales_details.htm');
            }
            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => sql2date($to, 'dd MMM yyyy'), 'grup' => $grup_name)));
            $this->TBS->MergeBlock('sales', $summary);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SalesDetails$from-$to.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionLaha()
    {
        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $tgl = $_POST['tglfrom'];
            $cash = U::report_cash_laha($tgl);
            $modal = U::report_modal($tgl);
            $vat_sales = U::report_vat_sales($tgl);
            $vat_retursales = U::report_vat_retursales($tgl);
            $disc_sales = U::report_discount_sales($tgl);
            $disc_retursales = U::report_discount_returnsales($tgl);
            $noncash = U::report_noncash_laha($tgl);
            $noncashsales = U::report_noncash_sales($tgl);
            $noncash_details = U::report_noncash_details_laha($tgl);
            $sales_laha = U::report_sales_laha($tgl);
            $retursales_laha = U::report_retursales_laha($tgl);
            $cashin = U::report_casin($tgl);
            $cashout = U::report_casout($tgl, 0);
            $cashoutother = U::report_casout($tgl, 1);
            $cash_before = U::get_balance_before_for_bank_account($tgl, Bank::get_bank_cash_id());
            $sum_sales_laha = array_sum(array_column($sales_laha, 'total'));
            $sum_cashin = array_sum(array_column($cashin, 'total'));
            $sum_retursales = array_sum(array_column($retursales_laha, 'total'));
            $sum_cashout = array_sum(array_column($cashout, 'total'));
            $sum_cashoutother = array_sum(array_column($cashoutother, 'total'));
            $sumall = $sum_sales_laha + $sum_cashin - $sum_retursales - $sum_cashout -
                $sum_cashoutother - $disc_sales + $disc_retursales + $vat_sales - $vat_retursales;
            $noncashsales = ($sum_sales_laha + $vat_sales - $disc_sales) - $noncashsales;
            $total_cashout = $sum_cashout + $sum_cashoutother;
            $pnl = $noncashsales - $total_cashout;
            $branch = STOREID;
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'laha.xml');
            } else {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'laha.htm');
            }
            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'branch' => $branch,
                'day' => date('l', $tgl), 'date' => $tgl, 'daily' => $sumall, 'sum_sales' => $sum_sales_laha,
                'cashdrawer' => $cash_before, 'modal' => $modal,
                'cashdrawerafter' => $modal + $cash_before + $pnl - $cash_before,
                'sales_vat_disc' => $sum_sales_laha + $vat_sales - $disc_sales,
                'noncashsales' => $noncashsales, 'total_cashout' => $total_cashout,
                'pnl' => $pnl,
                'sum_retursales' => $sum_retursales, 'sum_cashin' => $sum_cashin, 'sum_cashout' => $sum_cashout,
                'sum_cashoutother' => $sum_cashoutother, 'cashnoncash' => $cash + $noncash,
                'noncashdetails' => array_sum(array_column($noncash_details, 'total')))));
            $this->TBS->MergeBlock('sales', $sales_laha);
            $this->TBS->MergeBlock('retursales', $retursales_laha);
            $this->TBS->MergeBlock('cashin', $cashin);
            $this->TBS->MergeBlock('cashout', $cashout);
            $this->TBS->MergeBlock('cashoutother', $cashoutother);
            $this->TBS->MergeBlock('money', array(array('cash' => $cash, 'noncash' => $noncash)));
            $this->TBS->MergeBlock('noncash', $noncash_details);
            $this->TBS->MergeBlock('d', array(array('sales' => $disc_sales, 'retursales' => $disc_retursales)));
            $this->TBS->MergeBlock('v', array(array('sales' => $vat_sales, 'retursales' => $vat_retursales)));
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "Laha$branch$tgl.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionGeneralLedger()
    {
        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $coa = $_POST['account_code'];
            $chart_account = ChartMaster::model()->findByPk($coa);
            $result = U::get_general_ledger($coa, $from, $to);
            $begin = U::get_gl_before($coa, $from);
            $begin_arr = array('tgl' => '', 'memo_' => 'INITIAL BALANCE',
                'Debit' => $begin > 0 ? $begin : '',
                'Credit' => $begin < 0 ? -$begin : '',
                'Balance' => $begin,
                'amount' => $begin
            );
            $gl[] = $begin_arr;
            foreach ($result as $newrow) {
                $move = $newrow['amount'];
                $newrow['Balance'] = $newrow['amount'] + $begin;
                $begin = $newrow['Balance'];
                $gl[] = $newrow;
            }
            $dataProvider = new CArrayDataProvider($gl, array(
                'id' => 'GeneralLedger',
                'pagination' => false
            ));
            $this->render('GeneralLedger', array('dp' => $dataProvider,
                'from' => sql2date($from, 'dd MMM yyyy'),
                'to' => sql2date($to, 'dd MMM yyyy'),
                'account_code' => $chart_account->account_code,
                'account_name' => $chart_account->account_name
            ));
            Yii::app()->end();
        }
    }
    public function actionGeneralJournal()
    {
        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $result = U::get_general_journal($from, $to);
            $dataProvider = new CArrayDataProvider($result, array(
                'id' => 'GeneralJournal',
                'pagination' => false
            ));
            $this->render('GeneralJournal', array('dp' => $dataProvider,
                'from' => sql2date($from, 'dd MMM yyyy'),
                'to' => sql2date($to, 'dd MMM yyyy')
            ));
            Yii::app()->end();
        }
    }
}