<?php
class TenderDetailsController extends GxController
{
    public function actionCreate()
    {
        $model = new TenderDetails;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['TenderDetails'][$k] = $v;
            }
            $model->attributes = $_POST['TenderDetails'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->tender_details_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'TenderDetails');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['TenderDetails'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['TenderDetails'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->tender_details_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->tender_details_id));
            }
        }
    }
    public function actionIndex()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $tender_id = $_POST['tender_id'];
            $criteria = new CDbCriteria();
            $criteria->addCondition('tender_id = :tender_id');
            $criteria->params = array('tender_id'=>$tender_id);
            $model = TenderDetails::model()->findAll($criteria);
            $total = TenderDetails::model()->count($criteria);
            $this->renderJson($model, $total);
        }else{
            $this->renderJsonArr(TenderDetails::getEmptyDetails());
        }

    }
}