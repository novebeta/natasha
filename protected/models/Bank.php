<?php

Yii::import('application.models._base.BaseBank');
class Bank extends BaseBank
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_bank_cash()
    {
        return Bank::model()->findByPk(SysPrefs::get_val('kas_cabang'));
    }
    public static function get_bank_cash_id()
    {
        $ret = self::get_bank_cash();
        if ($ret != null) {
            return $ret->bank_id;
        }
        throw new Exception(t('bank.fail.cash', 'app'));
        return -1;
    }
    public function beforeValidate()
    {
        if($this->isNewRecord){
            $this->bank_id = U::generate_primary_key(RBANK);
        }
        if ($this->store == null) $this->store = STOREID;
        return parent::beforeValidate();
    }
}