<?php

Yii::import('application.models._base.BaseBankTrans');
class BankTrans extends BaseBankTrans
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        $this->tdate = new CDbExpression('NOW()');
        $this->store = STOREID;
        $this->id_user = Yii::app()->user->getId();
        return parent::beforeValidate();
    }
    public static function get_balance($id)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT IFNULL(SUM(nbt.amount),0) AS total
        FROM nscc_bank_trans nbt
        WHERE nbt.bank_id = :bank_id");
        return $comm->queryScalar(array(':bank_id' => $id));
    }
}