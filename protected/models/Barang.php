<?php

Yii::import('application.models._base.BaseBarang');

class Barang extends BaseBarang
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public static function count_biaya_beli($barang_id,$unit,$harga){
        $barang = Barang::model()->findByPk($barang_id);
        $jml_stok = StockMoves::get_saldo_item($barang_id);
        $harga_lama = $barang->cost;
        $total_lama = $jml_stok * $harga_lama;
        $total_baru = $harga;
        $jml_baru = $jml_stok + $unit;
        $harga_baru = ($total_baru + $total_lama) / $jml_baru;
        $barang->cost = $harga_baru;
        if(!$barang->save()){
            throw new Exception(t('save.fail','app').CHtml::errorSummary($barang));
        }
    }
    public function beforeValidate()
    {
        if($this->isNewRecord){
            $this->barang_id = U::generate_primary_key(RBARANG);
        }
        if ($this->store == null) $this->store = STOREID;
        return parent::beforeValidate();
    }
}