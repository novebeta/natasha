<?php

Yii::import('application.models._base.BaseBeli');
class Beli extends BaseBeli
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function save_default_price($barang_id, $price, $tax, $store)
    {
        $comm = Yii::app()->db->createCommand(
            "REPLACE INTO nscc_beli (price, tax, store, up, barang_id)
                VALUES (:price, :tax, :store, 0, :barang_id)"
        );
        return $comm->execute(array(':price' => $price, ':tax' => $tax, ':store' => $store, ':barang_id' => $barang_id));
    }
    public static function save_default_price_by_region($barang_id, $price, $tax, $wilayah_id)
    {
        $comm = Yii::app()->db->createCommand(
            "REPLACE INTO nscc_beli (price, tax, store, up, barang_id)
            SELECT $price as price, $tax as tax, ns.store_kode as store,0 as up,'$barang_id' as barang_id
				 FROM nscc_store ns WHERE ns.wilayah_id = :wilayah_id;"
        );
        return $comm->execute(array(':wilayah_id' => $wilayah_id));
    }
    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->beli_id = U::generate_primary_key(RBELI);
        }
        if ($this->store == null) $this->store = STOREID;
        return parent::beforeValidate();
    }
}