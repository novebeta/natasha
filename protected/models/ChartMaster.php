<?php

Yii::import('application.models._base.BaseChartMaster');

class ChartMaster extends BaseChartMaster
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public static function get_child($coa,$inc_header = false){
        $res = array();
        $criteria = new CDbCriteria();
        $criteria->addCondition("kategori = :account_code");
        $criteria->params = array(':account_code' => $coa);
        $chart = ChartMaster::model()->findAll($criteria);
        foreach($chart as $c){
            if($c->header == 1){
                if($inc_header){
                    $res[] = $c;
                }
            }else{
                $res[] = $c;
            }
            $childArr = Self::get_child($c->account_code,$inc_header);
            $res = array_merge($res,$childArr);
        }
        return $res;
    }
}