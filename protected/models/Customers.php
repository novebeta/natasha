<?php

Yii::import('application.models._base.BaseCustomers');

class Customers extends BaseCustomers
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public static function get_backup(){
        $res = Yii::app()->db->createCommand("
        SELECT CONCAT(nc.nama_customer, ' ', nc.no_customer) nama,
  nc.alamat, nc.customer_id noax, nkota.nama_kota kota,
  nc.telp, DATE(nc.awal) awal,
  if(nc.sex = 'MALE','PRIA','WANITA') sex,
  nc.tgl_lahir tgllh, nc.kerja
FROM nscc_customers nc
  INNER JOIN nscc_kecamatan nk
    ON nc.kecamatan_id = nk.kecamatan_id
  INNER JOIN nscc_kota nkota
    ON nk.kota_id = nkota.kota_id
    WHERE nc.log = 0");
        return $res->queryAll(true);
    }
}