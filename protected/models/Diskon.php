<?php

Yii::import('application.models._base.BaseDiskon');

class Diskon extends BaseDiskon
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public static function save_diskon($barang_id,$status_cust_id,$value){
        $comm = Yii::app()->db->createCommand(
            "REPLACE INTO `{{diskon}}` (`value`, `barang_id`, `status_cust_id`) VALUES (:value,:barang_id,:status_cust_id)"
        );
        return $comm->execute(array(':value'=>$value,'barang_id'=>$barang_id,':status_cust_id'=>$status_cust_id));
    }
    public static function save_diskon_by_grup($status_cust_id,$grup,$value){
        $comm = Yii::app()->db->createCommand(
            "REPLACE INTO {{diskon}} (`status_cust_id`,`barang_id`,`value`)
            SELECT $status_cust_id as status_cust_id,nb.barang_id,
            $value as `value` FROM nscc_barang nb WHERE nb.grup_id = :grup_id"
        );
        return $comm->execute(array('grup_id'=>$grup));
    }
}