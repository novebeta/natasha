<?php

Yii::import('application.models._base.BaseGlTrans');
class GlTrans extends BaseGlTrans
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        $this->tdate = new CDbExpression('NOW()');
        if ($this->store == null) $this->store = STOREID;
        return parent::beforeValidate();
    }
}