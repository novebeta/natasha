<?php

Yii::import('application.models._base.BaseGrup');

class Grup extends BaseGrup
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if($this->isNewRecord){
            $this->grup_id = U::generate_primary_key(RGRUP);
        }
        if ($this->store == null) $this->store = STOREID;
        return parent::beforeValidate();
    }
}