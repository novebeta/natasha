<?php

Yii::import('application.models._base.BaseKas');
Yii::import('application.components.U');
class Kas extends BaseKas
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if($this->isNewRecord){
            $this->kas_id = U::generate_primary_key(RKAS);
        }
        $this->tdate = new CDbExpression('NOW()');
        $this->store = STOREID;
        $this->user_id = Yii::app()->user->getId();
        return parent::beforeValidate();
    }
    public static function is_modal_exist($tgl){
        $criteria = new CDbCriteria();
        $criteria->addCondition("total > 0");
        $criteria->addCondition("type_ = 1");
        $criteria->addCondition("DATE(tgl) = :tgl");
        $criteria->params = array(':tgl' => $tgl);
        $res = Kas::model()->count($criteria);
        return $res > 0;
    }
//    protected function afterSave()
//    {
//        parent::afterSave();
//        $is_in = $this->total >= 0;
//        U::add_bank_trans($is_in ? CASHIN : CASHOUT, $this->kas_id, $this->bank_id, $this->doc_ref, $this->tgl,
//            $this->total, $this->user_id);
//    }
}