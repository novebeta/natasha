<?php

Yii::import('application.models._base.BasePayment');

class Payment extends BasePayment
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public static function add_payment($bank,$salestrans_id,$card_number,$amount){
        $pay = new Payment;
        $pay->bank_id = $bank;
        $pay->salestrans_id = $salestrans_id;
        $pay->card_number = $card_number;
        $pay->amount = $amount;
        if(!$pay->save()){
            throw new Exception(t('save.fail','app').CHtml::errorSummary($pay));
        }
    }
}