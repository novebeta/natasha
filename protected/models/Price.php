<?php

Yii::import('application.models._base.BasePrice');
class Price extends BasePrice
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function save_price($barang_id, $gol_id, $value)
    {
        $comm = Yii::app()->db->createCommand(
            "REPLACE INTO `{{price}}`(`value`, `barang_id`, `gol_id`, `store`) VALUES (:value,:barang_id,:gol_id,:store)"
        );
        return $comm->execute(array(':value' => $value, ':barang_id' => $barang_id, ':gol_id' => $gol_id, ':store' => STOREID));
    }
    public static function get_price($barang_id, $gol_id)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("barang_id = :barang_id");
        $criteria->addCondition("gol_id = :gol_id");
        $criteria->params = array(':barang_id' => $barang_id, ':gol_id' => $gol_id);
        return Price::model()->find($criteria);
    }
    public static function save_price_by_grup($grup_id, $gol_id, $value)
    {
        $store = STOREID;
        $comm = Yii::app()->db->createCommand(
            "REPLACE INTO nscc_price (`barang_id`,`value`,`gol_id`,`store`)
            SELECT nb.barang_id, $value as `value`, $gol_id as gol_id,'$store' as `store`
				 FROM nscc_barang nb WHERE nb.grup_id = :grup_id;"
        );
        return $comm->execute(array('grup_id' => $grup_id));
    }
}