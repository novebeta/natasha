<?php

Yii::import('application.models._base.BaseSalestrans');
class Salestrans extends BaseSalestrans
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if($this->isNewRecord){
            $this->salestrans_id = U::generate_primary_key(RSALESTRANS);
        }
        $this->tdate = new CDbExpression('NOW()');
        $this->user_id = Yii::app()->user->getId();
        return parent::beforeValidate();
    }
    public static function list_beuty_service($tgl)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT ns.salestrans_id,ns.doc_ref,nsd.salestrans_details,nsd.barang_id,
nsd.qty,nsd.jasa_dokter,nsd.dokter_id,nb.kode_barang,nb.nama_barang,
  IFNULL((SELECT nsb.final FROM nscc_beauty_services nsb
WHERE nsb.salestrans_details = nsd.salestrans_details LIMIT 1),0) final,
(SELECT nsb.beauty_id FROM nscc_beauty_services nsb
WHERE nsb.salestrans_details = nsd.salestrans_details LIMIT 1) beauty_id,
(SELECT nsb.beauty_id FROM nscc_beauty_services nsb
WHERE nsb.salestrans_details = nsd.salestrans_details LIMIT 1,1) beauty2_id
FROM nscc_salestrans AS ns
INNER JOIN nscc_salestrans_details AS nsd ON nsd.salestrans_id = ns.salestrans_id
INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
INNER JOIN nscc_grup AS ng ON nb.grup_id = ng.grup_id
WHERE ng.kategori_id = 1 AND ns.total >= 0 AND DATE(ns.tgl) = :tgl");
        return $comm->queryAll(true, array(':tgl' => $tgl));
    }
    public static function get_trans($tgl)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT ns.*, nc.nama_customer, nc.no_customer
        FROM nscc_salestrans ns
        INNER JOIN nscc_customers nc
            ON ns.customer_id = nc.customer_id
            WHERE DATE(ns.tgl) = :tgl AND ns.bruto >= 0 AND ns.log = 0");
        return $comm->queryAll(true, array(':tgl' => $tgl));
    }
    public static function get_trans_history($tgl)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT ns.*, nc.nama_customer, nc.no_customer
        FROM nscc_salestrans ns
        INNER JOIN nscc_customers nc
            ON ns.customer_id = nc.customer_id
            WHERE DATE(ns.tgl) = :tgl AND ns.bruto >= 0 AND ns.log = 1");
        return $comm->queryAll(true, array(':tgl' => $tgl));
    }
    public static function get_retursales_trans($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT  ns.salestrans_id,  ns.tgl,  ns.doc_ref,  ns.tdate,
  ns.user_id,  - ns.bruto AS bruto,  - ns.disc AS disc,  - ns.discrp AS discrp,  - ns.totalpot AS totalpot,
  - ns.total AS total,  ns.ketdisc,  - ns.vat AS vat,  ns.doc_ref_sales,  ns.audit,  ns.store,  ns.printed,
  ns.override,  nc.nama_customer,  nc.no_customer,  ns.customer_id,  - ns.bayar AS bayar,  - ns.kembali AS kembali,
  np.bank_id
FROM nscc_salestrans ns
  INNER JOIN nscc_customers nc
    ON ns.customer_id = nc.customer_id
  INNER JOIN nscc_payment np
    ON np.salestrans_id = ns.salestrans_id
WHERE DATE(ns.tgl) = :tgl AND ns.bruto <= 0");
        return $comm->queryAll(true, array(':tgl' => $tgl));
    }
    public static function set_final_service($tgl)
    {
        $comm = Yii::app()->db->createCommand("UPDATE {{BeautyServices}} as bs
            INNER JOIN {{salestrans_details}} AS nsd ON nsd.salestrans_details = bs.salestrans_details
            INNER JOIN {{salestrans}} AS ns ON nsd.salestrans_id = ns.salestrans_id
            INNER JOIN {{barang}} AS nb ON nsd.barang_id = nb.barang_id
            INNER JOIN {{grup}} AS ng ON nb.grup_id = ng.grup_id
            SET bs.final = 1
            WHERE ng.kategori_id = 1 AND ns.total >= 0 AND DATE(ns.tgl) = :tgl
        ");
        return $comm->execute(array(':tgl' => $tgl));
    }
    public static function get_backup(){
        $res = Yii::app()->db->createCommand("
       SELECT nb.kode_barang kodebrg, nsd.qty,
  CONCAT(nc.nama_customer,' ',nc.no_customer) nama,
  nsd.ketpot komen, ns.ketdisc komen_resep,
  ns.doc_ref resep, nb.sat satuan,
  date(ns.tgl) tanggal
FROM nscc_salestrans_details nsd
  INNER JOIN nscc_salestrans ns
    ON nsd.salestrans_id = ns.salestrans_id
  INNER JOIN nscc_customers nc
    ON ns.customer_id = nc.customer_id
  INNER JOIN nscc_barang nb
    ON nsd.barang_id = nb.barang_id
WHERE ns.bruto > 0 AND ns.log = 0");
        return $res->queryAll(true);
    }
}