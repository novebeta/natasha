<?php

Yii::import('application.models._base.BaseStockMoves');

class StockMoves extends BaseStockMoves
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        $this->tdate = new CDbExpression('NOW()');
        $this->store = STOREID;
        $this->id_user = Yii::app()->user->getId();
        return parent::beforeValidate();
    }
    public static function get_saldo_item_before($barang_id,$tgl){
        $comm = Yii::app()->db->createCommand("SELECT
        IFNULL(Sum(nsm.qty),0) FROM nscc_stock_moves AS nsm
        WHERE nsm.barang_id = :barang_id AND nsm.tran_date < :tgl");
        return $comm->queryScalar(array(':barang_id'=>$barang_id,':tgl'=>$tgl));
    }
    public static function get_saldo_item($barang_id){
        $comm = Yii::app()->db->createCommand("SELECT
        IFNULL(Sum(nsm.qty),0) FROM nscc_stock_moves AS nsm
        WHERE nsm.barang_id = :barang_id");
        return $comm->queryScalar(array(':barang_id'=>$barang_id));
    }
}