<?php

Yii::import('application.models._base.BaseSysPrefs');
class SysPrefs extends BaseSysPrefs
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_val($index)
    {
        $pref = SysPrefs::model()->find('name_ = :name_ AND store = :store',
            array(':name_' => $index, ':store' => STOREID));
        return $pref->value_;
    }
}