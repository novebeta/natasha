<?php

Yii::import('application.models._base.BaseTender');
class Tender extends BaseTender
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if($this->isNewRecord){
            $this->tender_id = U::generate_primary_key(RTENDER);
        }
        $this->tdate = new CDbExpression('NOW()');
        $this->store = STOREID;
        $this->user_id = Yii::app()->user->getId();
        return parent::beforeValidate();
    }
    public static function is_exist($tgl){
        $tender = Tender::model()->find('DATE(tgl) = :tgl',array(':tgl'=>$tgl));
        return $tender != null;
    }
}