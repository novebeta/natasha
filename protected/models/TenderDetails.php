<?php

Yii::import('application.models._base.BaseTenderDetails');

class TenderDetails extends BaseTenderDetails
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public static function getEmptyDetails(){
        $comm = Yii::app()->db->createCommand("
        SELECT b.bank_id,0 amount FROM {{bank}} b
        ");
        return $comm->queryAll(true);
    }
}