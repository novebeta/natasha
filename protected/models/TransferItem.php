<?php

Yii::import('application.models._base.BaseTransferItem');
class TransferItem extends BaseTransferItem
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->transfer_item_id = U::generate_primary_key(RTRANSFERITEM);
        }
        $this->tdate = new CDbExpression('NOW()');
        if ($this->store == null) $this->store = STOREID;
        $this->user_id = Yii::app()->user->getId();
        return parent::beforeValidate();
    }
}