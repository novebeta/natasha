<?php

Yii::import('application.models._base.BaseUsers');

class Users extends BaseUsers
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public static function is_audit(){
        $id = Yii::app()->user->getId();
        $user = Users::model()->findByPk($id);
        return strtolower(substr($user->user_id,0,3)) === strtolower(SysPrefs::get_val('audit')) ? 1 : 0;
    }
    public static function get_override($user,$pass){
        $comm = Yii::app()->db->createCommand("SELECT ns.id FROM nscc_users AS ns
        INNER JOIN nscc_security_roles AS nsc ON ns.security_roles_id = nsc.security_roles_id
        WHERE ns.`password` = :pass AND ns.user_id = :user AND nsc.sections LIKE '%209%'");
        return $comm->queryScalar(array(':pass'=>$pass,':user'=>$user));
    }
    public function beforeValidate()
    {
        if($this->isNewRecord){
            $this->id = U::generate_primary_key(RUSERS);
        }
        if ($this->store == null) $this->store = STOREID;
        return parent::beforeValidate();
    }
}