<?php

/**
 * This is the model base class for the table "{{barang}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Barang".
 *
 * Columns in table "{{barang}}" available as properties of the model,
 * followed by relations of table "{{barang}}" available as properties of the model.
 *
 * @property string $barang_id
 * @property string $kode_barang
 * @property string $nama_barang
 * @property string $ket
 * @property string $grup_id
 * @property integer $active
 * @property double $price
 * @property string $sat
 * @property double $cost
 * @property string $store
 * @property integer $up
 *
 * @property AuditDetails[] $auditDetails
 * @property Grup $grup
 * @property Diskon[] $diskons
 * @property Price[] $prices
 * @property SalestransDetails[] $salestransDetails
 * @property StockMoves[] $stockMoves
 * @property TransferItemDetails[] $transferItemDetails
 */
abstract class BaseBarang extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{barang}}';
	}

	public static function representingColumn() {
		return 'kode_barang';
	}

	public function rules() {
		return array(
			array('barang_id, kode_barang, nama_barang, grup_id, sat, store', 'required'),
			array('active, up', 'numerical', 'integerOnly'=>true),
			array('price, cost', 'numerical'),
			array('barang_id, ket, grup_id', 'length', 'max'=>50),
			array('kode_barang', 'length', 'max'=>15),
			array('nama_barang', 'length', 'max'=>30),
			array('sat', 'length', 'max'=>10),
			array('store', 'length', 'max'=>20),
			array('ket, active, price, cost, up', 'default', 'setOnEmpty' => true, 'value' => null),
			array('barang_id, kode_barang, nama_barang, ket, grup_id, active, price, sat, cost, store, up', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'auditDetails' => array(self::HAS_MANY, 'AuditDetails', 'barang_id'),
			'grup' => array(self::BELONGS_TO, 'Grup', 'grup_id'),
			'diskons' => array(self::HAS_MANY, 'Diskon', 'barang_id'),
			'prices' => array(self::HAS_MANY, 'Price', 'barang_id'),
			'salestransDetails' => array(self::HAS_MANY, 'SalestransDetails', 'barang_id'),
			'stockMoves' => array(self::HAS_MANY, 'StockMoves', 'barang_id'),
			'transferItemDetails' => array(self::HAS_MANY, 'TransferItemDetails', 'barang_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'barang_id' => Yii::t('app', 'Barang'),
			'kode_barang' => Yii::t('app', 'Kode Barang'),
			'nama_barang' => Yii::t('app', 'Nama Barang'),
			'ket' => Yii::t('app', 'Ket'),
			'grup_id' => Yii::t('app', 'Grup'),
			'active' => Yii::t('app', 'Active'),
			'price' => Yii::t('app', 'Price'),
			'sat' => Yii::t('app', 'Sat'),
			'cost' => Yii::t('app', 'Cost'),
			'store' => Yii::t('app', 'Store'),
			'up' => Yii::t('app', 'Up'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('barang_id', $this->barang_id, true);
		$criteria->compare('kode_barang', $this->kode_barang, true);
		$criteria->compare('nama_barang', $this->nama_barang, true);
		$criteria->compare('ket', $this->ket, true);
		$criteria->compare('grup_id', $this->grup_id);
		$criteria->compare('active', $this->active);
		$criteria->compare('price', $this->price);
		$criteria->compare('sat', $this->sat, true);
		$criteria->compare('cost', $this->cost);
		$criteria->compare('store', $this->store, true);
		$criteria->compare('up', $this->up);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}