<?php

/**
 * This is the model base class for the table "{{beli}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Beli".
 *
 * Columns in table "{{beli}}" available as properties of the model,
 * followed by relations of table "{{beli}}" available as properties of the model.
 *
 * @property integer $beli_id
 * @property double $price
 * @property double $tax
 * @property string $store
 * @property integer $up
 * @property string $barang_id
 *
 * @property Barang $barang
 */
abstract class BaseBeli extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{beli}}';
	}

	public static function representingColumn() {
		return 'store';
	}

	public function rules() {
		return array(
			array('store, barang_id', 'required'),
			array('up', 'numerical', 'integerOnly'=>true),
			array('price, tax', 'numerical'),
			array('store', 'length', 'max'=>20),
			array('barang_id', 'length', 'max'=>50),
			array('price, tax, up', 'default', 'setOnEmpty' => true, 'value' => null),
			array('beli_id, price, tax, store, up, barang_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'barang' => array(self::BELONGS_TO, 'Barang', 'barang_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'beli_id' => Yii::t('app', 'Beli'),
			'price' => Yii::t('app', 'Price'),
			'tax' => Yii::t('app', 'Tax'),
			'store' => Yii::t('app', 'Store'),
			'up' => Yii::t('app', 'Up'),
			'barang_id' => Yii::t('app', 'Barang'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('beli_id', $this->beli_id);
		$criteria->compare('price', $this->price);
		$criteria->compare('tax', $this->tax);
		$criteria->compare('store', $this->store, true);
		$criteria->compare('up', $this->up);
		$criteria->compare('barang_id', $this->barang_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}