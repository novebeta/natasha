<?php

/**
 * This is the model base class for the table "{{wilayah}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Wilayah".
 *
 * Columns in table "{{wilayah}}" available as properties of the model,
 * followed by relations of table "{{wilayah}}" available as properties of the model.
 *
 * @property integer $wilayah_id
 * @property string $kode_wilayah
 * @property string $nama_wilayah
 *
 * @property Store[] $stores
 */
abstract class BaseWilayah extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{wilayah}}';
	}

	public static function representingColumn() {
		return 'kode_wilayah';
	}

	public function rules() {
		return array(
			array('kode_wilayah, nama_wilayah', 'required'),
			array('kode_wilayah', 'length', 'max'=>20),
			array('nama_wilayah', 'length', 'max'=>50),
			array('wilayah_id, kode_wilayah, nama_wilayah', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'stores' => array(self::HAS_MANY, 'Store', 'wilayah_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'wilayah_id' => Yii::t('app', 'Wilayah'),
			'kode_wilayah' => Yii::t('app', 'Kode Wilayah'),
			'nama_wilayah' => Yii::t('app', 'Nama Wilayah'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('wilayah_id', $this->wilayah_id);
		$criteria->compare('kode_wilayah', $this->kode_wilayah, true);
		$criteria->compare('nama_wilayah', $this->nama_wilayah, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}