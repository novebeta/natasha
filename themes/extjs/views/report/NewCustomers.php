<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>New Customers</title>
    <style type="text/css">
        body {
            font-family: helvetica, tahoma, verdana, sans-serif;
            padding: 20px;
            padding-top: 32px;
            font-size: 13px;
            background-color: #F8DBE6 !important;
            color: #8b1d51 !important;
        }
        .grid-view table.items {
            background: white;
            border-collapse: collapse;
            width: 100%;
            border: 1px #a83a6e solid !important;
        }

        .grid-view table.items th, .grid-view table.items td {
            color: #8b1d51 !important;
            font-size: 0.9em;
            border: 1px white solid;
            border-color: #ba4c80 #a83a6e #a83a6e !important;
            padding: 0.3em;
        }

        .grid-view table.items th {
            color: #8b1d51 !important;
            background: #ffe8ff !important;
            text-align: center;
        }

        .grid-view table.items th a {
            color: #EEE;
            font-weight: bold;
            text-decoration: none;
        }

        .grid-view table.items th a:hover {
            color: #FFF;
        }
        .grid-view table.items tr.even {
            background: #ecceed;
        }

        .grid-view table.items tr.odd {
            background: #ffe8ff;
        }

        .grid-view table.items tr.selected {
            background: #cbadcc;
            border-color: #67002d;
        }

        .grid-view table.items tr:hover.selected {
            border-color: #982a5e;
            background: #eed0ef;
        }

        .grid-view table.items tbody tr:hover {
            background: #FFF5FD;
        }

        .grid-view .link-column img {
            border: 0;
        }

        .grid-view .button-column {
            text-align: center;
            width: 60px;
        }

        .grid-view .button-column img {
            border: 0;
        }

        .grid-view .checkbox-column {
            width: 15px;
        }

        .grid-view .summary {
            margin: 0 0 5px 0;
            text-align: right;
        }

        .grid-view .pager {
            margin: 5px 0 0 0;
            text-align: right;
        }

        .grid-view .empty {
            font-style: italic;
        }

        .grid-view .filters input,
        .grid-view .filters select {
            width: 100%;
            border: 1px solid #ccc;
        }

        /* grid border */
        .grid-view table.items th, .grid-view table.items td {
            border: 1px solid #a83a6e !important;
        }

        /* disable selection for extrarows */
        .grid-view td.extrarow {
            background: none repeat scroll 0 0 #F8F8F8;
        }

        .subtotal {
            font-size: 14px;
            color: brown;
            font-weight: bold;
        }
    </style>
</head>
<body>
<div style="height: 100px; left: 0px; top: 0px;">
    <img alt="" src="<?= bu() . "/images/logo.png" ?>">
</div>
<h1>New Customers</h1>
<h3>FROM : <?=$from?></h3>
<h3>TO : <?=$to?></h3>
<?
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'No. Customers',
            'name' => 'no_customer'
        ),
        array(
            'header' => 'Customers Name',
            'name' => 'nama_customer'
        ),
        array(
            'header' => 'Birth Place',
            'name' => 'tempat_lahir'
        ),
        array(
            'header' => 'BirthDay',
            'name' => 'tgl_lahir'
        ),
        array(
            'header' => 'Email',
            'name' => 'email'
        ),
        array(
            'header' => 'Phone',
            'name' => 'telp'
        ),
        array(
            'header' => 'First',
            'name' => 'awal'
        ),
        array(
            'header' => 'Last',
            'name' => 'akhir'
        ),
        array(
            'header' => 'Origin Branch',
            'name' => 'store'
        ),
        array(
            'header' => 'Address',
            'name' => 'alamat'
        ),
        array(
            'header' => 'Sub District',
            'name' => 'nama_kecamatan'
        ),
        array(
            'header' => 'City',
            'name' => 'nama_kota'
        ),
        array(
            'header' => 'Province',
            'name' => 'nama_provinsi'
        ),
        array(
            'header' => 'Country',
            'name' => 'nama_negara'
        ),
        array(
            'header' => 'Status',
            'name' => 'nama_status'
        ),
    )
));
?>
</body>
</html>