<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>Tenders</title>
    <style type="text/css">
        body {
            font-family: helvetica, tahoma, verdana, sans-serif;
            padding: 20px;
            padding-top: 32px;
            font-size: 13px;
            background-color: #F8DBE6 !important;
            color: #8b1d51 !important;
        }

        .grid-view table.items {
            background: white;
            border-collapse: collapse;
            width: 100%;
            border: 1px #a83a6e solid !important;
        }

        .grid-view table.items th, .grid-view table.items td {
            color: #8b1d51 !important;
            font-size: 0.9em;
            border: 1px white solid;
            border-color: #ba4c80 #a83a6e #a83a6e !important;
            padding: 0.3em;
        }

        .grid-view table.items th {
            color: #8b1d51 !important;
            background: #ffe8ff !important;
            text-align: center;
        }

        .grid-view table.items th a {
            color: #EEE;
            font-weight: bold;
            text-decoration: none;
        }

        .grid-view table.items th a:hover {
            color: #FFF;
        }

        .grid-view table.items tr.even {
            background: #ecceed;
        }

        .grid-view table.items tr.odd {
            background: #ffe8ff;
        }

        .grid-view table.items tr.selected {
            background: #cbadcc;
            border-color: #67002d;
        }

        .grid-view table.items tr:hover.selected {
            border-color: #982a5e;
            background: #eed0ef;
        }

        .grid-view table.items tbody tr:hover {
            background: #FFF5FD;
        }

        .grid-view .link-column img {
            border: 0;
        }

        .grid-view .button-column {
            text-align: center;
            width: 60px;
        }

        .grid-view .button-column img {
            border: 0;
        }

        .grid-view .checkbox-column {
            width: 15px;
        }

        .grid-view .summary {
            margin: 0 0 5px 0;
            text-align: right;
        }

        .grid-view .pager {
            margin: 5px 0 0 0;
            text-align: right;
        }

        .grid-view .empty {
            font-style: italic;
        }

        .grid-view .filters input,
        .grid-view .filters select {
            width: 100%;
            border: 1px solid #ccc;
        }

        /* grid border */
        .grid-view table.items th, .grid-view table.items td {
            border: 1px solid #a83a6e !important;
        }

        /* disable selection for extrarows */
        .grid-view td.extrarow {
            background: none repeat scroll 0 0 #F8F8F8;
        }

        .subtotal {
            font-size: 14px;
            color: brown;
            font-weight: bold;
        }
    </style>
</head>
<body>
<div style="height: 100px; left: 0px; top: 0px;">
    <img alt="" src="<?= bu() . "/images/logo.png" ?>">
</div>
<h1>Tenders</h1>

<h3>Date : <?= $start; ?></h3>
<?
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Payment Method',
            'name' => 'nama_bank',
            'footerHtmlOptions' => array ('style' => 'text-align: center;font-weight:bold;' ),
            'footer' => $tender
        ),
        array(
            'header' => 'System',
            'name' => 'system',
            'value' => function ($data) {
                    return number_format($data['system'], 2);
                },
            'htmlOptions' => array ('style' => 'text-align: right;' )
        ),
        array(
            'header' => 'Tendered',
            'name' => 'tenders',
            'value' => function ($data) {
                    return number_format($data['tenders'], 2);
                },
            'htmlOptions' => array ('style' => 'text-align: right;' ),
            'footerHtmlOptions' => array ('style' => 'text-align: right;font-weight:bold;' ),
            'footer' => number_format($selisih,2)
        ),
    ),
));
?>
</body>
</html>